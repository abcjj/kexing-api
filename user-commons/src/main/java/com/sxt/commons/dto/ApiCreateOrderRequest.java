package com.sxt.commons.dto;

import java.io.Serializable;

/**
 * 普通接口请求的参数实体
 * 定义此类型为统一规范。
 */
public class ApiCreateOrderRequest implements Serializable {
	private String body;//描述
	private String mchCreateIp;//商户--终端ip
	private String mchId;//商户号
	private String nonceStr;//随机字符串
	private String notifyUrl;//通知地址
	private String returnUrl;//前台跳转地址
	private String outTradeNo;//商户订单号
	private Integer totalFee;//交易金额，单位：分
	private Byte type;//接口类型
	private String attach;//附加信息，原路返回
	private String sign;//签名

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getMchCreateIp() {
		return mchCreateIp;
	}

	public void setMchCreateIp(String mchCreateIp) {
		this.mchCreateIp = mchCreateIp;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public Integer getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Integer totalFee) {
		this.totalFee = totalFee;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return "ApiCreateOrderRequest{" +
				"body='" + body + '\'' +
				", mchCreateIp='" + mchCreateIp + '\'' +
				", mchId='" + mchId + '\'' +
				", nonceStr='" + nonceStr + '\'' +
				", notifyUrl='" + notifyUrl + '\'' +
				", returnUrl='" + returnUrl + '\'' +
				", outTradeNo='" + outTradeNo + '\'' +
				", totalFee=" + totalFee +
				", type=" + type +
				", attach='" + attach + '\'' +
				", sign='" + sign + '\'' +
				'}';
	}
}
