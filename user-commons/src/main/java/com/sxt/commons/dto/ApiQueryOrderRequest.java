package com.sxt.commons.dto;

import java.io.Serializable;

/**
 * 普通接口请求的参数实体
 * 定义此类型为统一规范。
 */
public class ApiQueryOrderRequest implements Serializable {
	private String mchId;//商户号
	private String outTradeNo;//商户订单号
	private String sign;//签名

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return "ApiQueryOrderRequest{" +
				"mchId='" + mchId + '\'' +
				", outTradeNo='" + outTradeNo + '\'' +
				", sign='" + sign + '\'' +
				'}';
	}
}
