package com.sxt.commons.dto;

import java.io.Serializable;

/**
 * UserProvider返回给Consumer的通用返回结果类型。
 * 定义此类型为统一规范。
 */
public class CallbackResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	// 状态编码， 如： 200 - 操作成功， 500 - 服务器错误， 400 - 客户端请求错误等。
	private int status;
	// 服务响应消息内容
	private String message;
	// 服务响应数据对象
	private Object data;
	public static CallbackResponse ok(){
		return new CallbackResponse(200, "处理成功", null);
	}
	public static CallbackResponse ok(Object data){
		return new CallbackResponse(200, "处理成功", data);
	}
	public static CallbackResponse ok(String message, Object data){
		return new CallbackResponse(200, message, data);
	}
	public static CallbackResponse build(int status, String message, Object data){
		return new CallbackResponse(status, message, data);
	}
	public static CallbackResponse error(){
		return new CallbackResponse(500, "处理失败", null);
	}
	public static CallbackResponse error(Object data){
		return new CallbackResponse(500, "处理失败", data);
	}
	public static CallbackResponse error(String message, Object data){
		return new CallbackResponse(500, message, data);
	}
	public CallbackResponse() {
		super();
	}
	public CallbackResponse(int status, String message, Object data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}
	public boolean isOk() {
		if(this.status==200)return true;
		else return false;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "{" +
				"status=" + status +
				", message='" + message + '\'' +
				", data=" + data +
				'}';
	}
}
