package com.sxt.commons.dto;

import java.util.Date;

public class CfgErrorMessage {
    /**
     *  消息编号,所属表字段为REG_CORE.CFG_ERROR_MESSAGE.MESSAGE_ID
     *
     * @mbggenerated
     */
    private Long messageId;

    /**
     *  错误消息代码,所属表字段为REG_CORE.CFG_ERROR_MESSAGE.MESSAGE_CODE
     *
     * @mbggenerated
     */
    private Long messageCode;

    /**
     *  错误模块,所属表字段为REG_CORE.CFG_ERROR_MESSAGE.ERROR_MODULE
     *
     * @mbggenerated
     */
    private String errorModule;

    /**
     *  错误消息,所属表字段为REG_CORE.CFG_ERROR_MESSAGE.ERROR_MESSAGE
     *
     * @mbggenerated
     */
    private String errorMessage;

    /**
     *  错误原因,所属表字段为REG_CORE.CFG_ERROR_MESSAGE.ERROR_REASON
     *
     * @mbggenerated
     */
    private String errorReason;

    /**
     *  建议处理方式,所属表字段为REG_CORE.CFG_ERROR_MESSAGE.SUGGUEST_MODE
     *
     * @mbggenerated
     */
    private String sugguestMode;

    /**
     *  修改时间,所属表字段为REG_CORE.CFG_ERROR_MESSAGE.MODIFY_DATE
     *
     * @mbggenerated
     */
    private Date modifyDate;

    /**
     *  修改用户,所属表字段为REG_CORE.CFG_ERROR_MESSAGE.MODIFY_USER_ID
     *
     * @mbggenerated
     */
    private String modifyUserId;

    /**
     *  创建时间,所属表字段为REG_CORE.CFG_ERROR_MESSAGE.CREATE_DATE
     *
     * @mbggenerated
     */
    private Date createDate;

    /**
     *  创建用户,所属表字段为REG_CORE.CFG_ERROR_MESSAGE.CREATE_USER_ID
     *
     * @mbggenerated
     */
    private String createUserId;

    /**
     * 获取 消息编号 字段:REG_CORE.CFG_ERROR_MESSAGE.MESSAGE_ID
     *
     * @return REG_CORE.CFG_ERROR_MESSAGE.MESSAGE_ID, 消息编号
     *
     * @mbggenerated
     */
    public Long getMessageId() {
        return messageId;
    }

    /**
     * 设置 消息编号 字段:REG_CORE.CFG_ERROR_MESSAGE.MESSAGE_ID
     *
     * @param messageId REG_CORE.CFG_ERROR_MESSAGE.MESSAGE_ID, 消息编号
     *
     * @mbggenerated
     */
    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    /**
     * 获取 错误消息代码 字段:REG_CORE.CFG_ERROR_MESSAGE.MESSAGE_CODE
     *
     * @return REG_CORE.CFG_ERROR_MESSAGE.MESSAGE_CODE, 错误消息代码
     *
     * @mbggenerated
     */
    public Long getMessageCode() {
        return messageCode;
    }

    /**
     * 设置 错误消息代码 字段:REG_CORE.CFG_ERROR_MESSAGE.MESSAGE_CODE
     *
     * @param messageCode REG_CORE.CFG_ERROR_MESSAGE.MESSAGE_CODE, 错误消息代码
     *
     * @mbggenerated
     */
    public void setMessageCode(Long messageCode) {
        this.messageCode = messageCode;
    }

    /**
     * 获取 错误模块 字段:REG_CORE.CFG_ERROR_MESSAGE.ERROR_MODULE
     *
     * @return REG_CORE.CFG_ERROR_MESSAGE.ERROR_MODULE, 错误模块
     *
     * @mbggenerated
     */
    public String getErrorModule() {
        return errorModule;
    }

    /**
     * 设置 错误模块 字段:REG_CORE.CFG_ERROR_MESSAGE.ERROR_MODULE
     *
     * @param errorModule REG_CORE.CFG_ERROR_MESSAGE.ERROR_MODULE, 错误模块
     *
     * @mbggenerated
     */
    public void setErrorModule(String errorModule) {
        this.errorModule = errorModule == null ? null : errorModule.trim();
    }

    /**
     * 获取 错误消息 字段:REG_CORE.CFG_ERROR_MESSAGE.ERROR_MESSAGE
     *
     * @return REG_CORE.CFG_ERROR_MESSAGE.ERROR_MESSAGE, 错误消息
     *
     * @mbggenerated
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * 设置 错误消息 字段:REG_CORE.CFG_ERROR_MESSAGE.ERROR_MESSAGE
     *
     * @param errorMessage REG_CORE.CFG_ERROR_MESSAGE.ERROR_MESSAGE, 错误消息
     *
     * @mbggenerated
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage == null ? null : errorMessage.trim();
    }

    /**
     * 获取 错误原因 字段:REG_CORE.CFG_ERROR_MESSAGE.ERROR_REASON
     *
     * @return REG_CORE.CFG_ERROR_MESSAGE.ERROR_REASON, 错误原因
     *
     * @mbggenerated
     */
    public String getErrorReason() {
        return errorReason;
    }

    /**
     * 设置 错误原因 字段:REG_CORE.CFG_ERROR_MESSAGE.ERROR_REASON
     *
     * @param errorReason REG_CORE.CFG_ERROR_MESSAGE.ERROR_REASON, 错误原因
     *
     * @mbggenerated
     */
    public void setErrorReason(String errorReason) {
        this.errorReason = errorReason == null ? null : errorReason.trim();
    }

    /**
     * 获取 建议处理方式 字段:REG_CORE.CFG_ERROR_MESSAGE.SUGGUEST_MODE
     *
     * @return REG_CORE.CFG_ERROR_MESSAGE.SUGGUEST_MODE, 建议处理方式
     *
     * @mbggenerated
     */
    public String getSugguestMode() {
        return sugguestMode;
    }

    /**
     * 设置 建议处理方式 字段:REG_CORE.CFG_ERROR_MESSAGE.SUGGUEST_MODE
     *
     * @param sugguestMode REG_CORE.CFG_ERROR_MESSAGE.SUGGUEST_MODE, 建议处理方式
     *
     * @mbggenerated
     */
    public void setSugguestMode(String sugguestMode) {
        this.sugguestMode = sugguestMode == null ? null : sugguestMode.trim();
    }

    /**
     * 获取 修改时间 字段:REG_CORE.CFG_ERROR_MESSAGE.MODIFY_DATE
     *
     * @return REG_CORE.CFG_ERROR_MESSAGE.MODIFY_DATE, 修改时间
     *
     * @mbggenerated
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置 修改时间 字段:REG_CORE.CFG_ERROR_MESSAGE.MODIFY_DATE
     *
     * @param modifyDate REG_CORE.CFG_ERROR_MESSAGE.MODIFY_DATE, 修改时间
     *
     * @mbggenerated
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * 获取 修改用户 字段:REG_CORE.CFG_ERROR_MESSAGE.MODIFY_USER_ID
     *
     * @return REG_CORE.CFG_ERROR_MESSAGE.MODIFY_USER_ID, 修改用户
     *
     * @mbggenerated
     */
    public String getModifyUserId() {
        return modifyUserId;
    }

    /**
     * 设置 修改用户 字段:REG_CORE.CFG_ERROR_MESSAGE.MODIFY_USER_ID
     *
     * @param modifyUserId REG_CORE.CFG_ERROR_MESSAGE.MODIFY_USER_ID, 修改用户
     *
     * @mbggenerated
     */
    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId == null ? null : modifyUserId.trim();
    }

    /**
     * 获取 创建时间 字段:REG_CORE.CFG_ERROR_MESSAGE.CREATE_DATE
     *
     * @return REG_CORE.CFG_ERROR_MESSAGE.CREATE_DATE, 创建时间
     *
     * @mbggenerated
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置 创建时间 字段:REG_CORE.CFG_ERROR_MESSAGE.CREATE_DATE
     *
     * @param createDate REG_CORE.CFG_ERROR_MESSAGE.CREATE_DATE, 创建时间
     *
     * @mbggenerated
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取 创建用户 字段:REG_CORE.CFG_ERROR_MESSAGE.CREATE_USER_ID
     *
     * @return REG_CORE.CFG_ERROR_MESSAGE.CREATE_USER_ID, 创建用户
     *
     * @mbggenerated
     */
    public String getCreateUserId() {
        return createUserId;
    }

    /**
     * 设置 创建用户 字段:REG_CORE.CFG_ERROR_MESSAGE.CREATE_USER_ID
     *
     * @param createUserId REG_CORE.CFG_ERROR_MESSAGE.CREATE_USER_ID, 创建用户
     *
     * @mbggenerated
     */
    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId == null ? null : createUserId.trim();
    }
}