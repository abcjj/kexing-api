package com.sxt.commons.exception;


/**
 * 自定义异常业务条件不满足，应用异常
 * Created by lzh on 2017/4/18 0018.
 */
public class AppException extends BaseException {

    public AppException(Long errCode) {
        super(errCode);
    }

    public AppException(Long errCode, Throwable cause) {
        super(errCode,cause);
    }

    public AppException(String errMsg, Throwable cause, String  advise) {
        super(errMsg, cause,advise);
    }

    public AppException(String errMsg, String errCause, String advise) {
        super(errMsg,errCause,advise);
    }

    public AppException(Long errCode, String transactionId, String nodeCode){
        super(errCode);
        this.setTransactionId(transactionId);
        this.setNodeCode(nodeCode);
    }

    public AppException(Long errCode, Throwable cause, String transactionId, String nodeCode){
        super(errCode,cause);
        this.setTransactionId(transactionId);
        this.setNodeCode(nodeCode);
    }
    public AppException(String errMsg, Throwable cause, String advise, String transactionId, String nodeCode){
        super(errMsg,cause,advise);
        this.setTransactionId(transactionId);
        this.setNodeCode(nodeCode);
    }
    public AppException(String errMsg, String errCause, String advise, String transactionId, String nodeCode){
        super(errMsg,errCause,advise);
        this.setTransactionId(transactionId);
        this.setNodeCode(nodeCode);
    }
}
