package com.sxt.commons.exception;


import com.sxt.commons.dto.CfgErrorMessage;
import com.sxt.commons.utils.CommUtil;
import com.sxt.commons.utils.ThrowableUtil;

/**
 * 自定义异常基类
 * Created by lzh on 2017/4/19 0019.
 */
public class BaseException extends RuntimeException {
    /**
     * 异常模块code
     */
    private Long errCode;
    /**
     * 异常错误信息
     */
    private String errMsg;
    /**
     * 异常错误原因
     */
    private String errCause;
    /**
     * 异常错误建议
     */
    private String advise;

    /**
     * 组装后的异常错误信息
     */
    private String installErr;
    /**
     * 案件编号
     */
    private String transactionId;
    /**
     * 工作流节点
     */
    private String nodeCode;

    /**
     * 调用模板
     *
     * @param errCode
     */
    public BaseException(Long errCode) {
        this.errCode = errCode;
        this.installErr = getErrorMsg(errCode, null);
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getNodeCode() {
        return nodeCode;
    }

    public void setNodeCode(String nodeCode) {
        this.nodeCode = nodeCode;
    }

    /**
     * 调用模板+异常信息
     *
     * @param errCode
     * @param cause
     */
    public BaseException(Long errCode, Throwable cause) {
        super(cause);
        this.errCode = errCode;
        this.installErr = getErrorMsg(errCode, cause);
    }

    /**
     * 错误信息+异常+建议
     *
     * @param errMsg
     * @param cause
     * @param advise
     */
    public BaseException(String errMsg, Throwable cause, String advise) {
        super(errMsg, cause);
        this.errMsg = errMsg;
        this.advise = advise;
        this.installErr = getErrorMsg(errMsg, cause, advise);
    }

    /**
     * 错误信息+错误原因+建议
     *
     * @param errMsg
     * @param errCause
     * @param advise
     */
    public BaseException(String errMsg, String errCause, String advise) {
        super(errMsg + "；原因：" + errCause + "；建议：" + advise);
        this.errMsg = errMsg;
        this.errCause = errCause;
        this.advise = advise;
        this.installErr = getErrorMsg(errMsg, errCause, advise);

    }

    public String getErrCause() {
        if (CommUtil.isEmpty(errCause) && super.getCause() != null) {
            return super.getMessage();
        }
        return errCause;
    }

    public void setErrCause(String errCause) {
        this.errCause = errCause;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Long getErrCode() {
        return errCode;
    }

    public void setErrCode(Long errCode) {
        this.errCode = errCode;
    }

    public String getAdvise() {
        return advise;
    }

    public void setAdvise(String advise) {
        this.advise = advise;
    }

    public String getInstallErr() {
        return installErr;
    }

    public void setInstallErr(String installErr) {
        this.installErr = installErr;
    }

    /**
     * 根据code从缓存中获取错误信息
     *
     * @param code
     * @return
     */
    private String getErrorMsg(Long code, Throwable cause) {
        String msg = "";
//        CfgErrorMessage cfgErrorMessage = CfgErrorMessageCache.getCache().getErrorMessage(code);
        CfgErrorMessage cfgErrorMessage =new CfgErrorMessage();
        if (cause != null) {
            msg = cfgErrorMessage.getErrorMessage() + "；原因：" + cause.getMessage() + "；建议：" + cfgErrorMessage.getSugguestMode();
        } else {
            msg = cfgErrorMessage.getErrorMessage() + "；原因：" + cfgErrorMessage.getErrorReason() + "；建议：" + cfgErrorMessage.getSugguestMode();
        }
        return msg;
    }

    /**
     * 带错误信息+异常错误原因+建议
     *
     * @param errMsg
     * @param cause
     * @param advise
     * @return
     */
    private String getErrorMsg(String errMsg, Throwable cause, String advise) {
        String msg = "";
        String exMsg = "";
        int explenth = 700;
        exMsg = cause.getMessage();
        if (CommUtil.isEmpty(exMsg)) {
            String exP = ThrowableUtil.getCause(cause);
            if (CommUtil.isNotEmpty(exP) && exP.length() > explenth) {
                exMsg = exP.substring(0, explenth) + "...";
            } else {
                exMsg = exP;
            }
        }
        msg = errMsg + "；原因：" + exMsg + "；建议：" + advise;
        return msg;
    }

    /**
     * 带错误信息+错误原因+建议
     *
     * @param errMsg
     * @param errCause
     * @param advise
     * @return
     */
    public String getErrorMsg(String errMsg, String errCause, String advise) {
        String msg = "";
        msg = errMsg + "；原因：" + errCause + "；建议：" + advise;
        return msg;
    }

    public String getMessage() {
        return this.installErr;
    }
}
