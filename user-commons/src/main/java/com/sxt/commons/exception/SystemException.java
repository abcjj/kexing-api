package com.sxt.commons.exception;

/**
 * 自定义异常-系统异常
 * Created by lzh on 2017/4/24 0024.
 */
public class SystemException extends BaseException {
    private String transactionId;//案件编号
    private String nodeCode;//工作流节点

    public SystemException(Long errCode) {
        super(errCode);
    }

    public SystemException(Long errCode, Throwable cause) {
        super(errCode,cause);
    }

    public SystemException(String errMsg, Throwable cause, String  advise) {
        super(errMsg, cause,advise);
    }

    public SystemException(String errMsg, String errCause, String advise) {
        super(errMsg,errCause,advise);
    }

    public SystemException(Long errCode, String transactionId, String nodeCode){
        super(errCode);
        this.setTransactionId(transactionId);
        this.setNodeCode(nodeCode);
    }

    public SystemException(Long errCode, Throwable cause, String transactionId, String nodeCode){
        super(errCode,cause);
        this.setTransactionId(transactionId);
        this.setNodeCode(nodeCode);
    }
    public SystemException(String errMsg, Throwable cause, String advise, String transactionId, String nodeCode){
        super(errMsg, cause, advise);
        this.setTransactionId(transactionId);
        this.setNodeCode(nodeCode);
    }
    public SystemException(String errMsg, String errCause, String advise, String transactionId, String nodeCode){
        super(errMsg,errCause,advise);
        this.setTransactionId(transactionId);
        this.setNodeCode(nodeCode);
    }
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getNodeCode() {
        return nodeCode;
    }

    public void setNodeCode(String nodeCode) {
        this.nodeCode = nodeCode;
    }
}
