package com.sxt.commons.typeEnum;

public enum ApiStatusEnum {
    OK(0, "OK"),
    FIAL(1, "FAIL"),
    ARGUMENT_ERR(10000001, "参数错误"),
    ARGUMENT_REQUEST(10000002, "缺少参数"),
    UNAUTHENTICATION(10000003, "用户未登录"),
    SIGN_ERR(10000004, "签名错误"),
    CP_CHANNEL_UNREGISTER(20000001, "商户未开通此种支付类型"),
    MCHID_LOGGIN_UNMATCH(20000002, "商户号与登录账号不一致"),
    CP_UNREGISTER(20000003, "商户未签约"),
    CP_FREEZE(20000004, "该商户已被冻结"),
    TRADE_NO_REQUEST(20000005, "订单不存在"),
    SP_ERR(30000001, "渠道异常"),
    SERVICE_BUSY(80000001, "系统繁忙，稍后在试"),
    SERVICE_ERR(88888888, "系统错误");

    private Integer code;
    private String info;

    ApiStatusEnum(Integer code, String info) {
        this.code = code;
        this.info = info;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String getInfo(Short code) {

        String info = "";

        for (ApiStatusEnum p : ApiStatusEnum.values()) {
            if (p.getCode().equals(code)) {
                info = p.getInfo();
                break;
            }
        }

        return info;
    }

    public static ApiStatusEnum getByType(Integer code) {
        for (ApiStatusEnum p : ApiStatusEnum.values()) {
            if (p.getCode().equals(code)) {
                return p;
            }
        }
        return SERVICE_ERR;
    }
}
