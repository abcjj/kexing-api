package com.sxt.commons.typeEnum;

// 对应着数据库中的api_type表数据
public enum ApiTypeEnum {
    WX_NATIVE(1, "微信统一下单API"),
    WX_TRADE_QUERY(2, "微信查询订单API"),
    WX_TRADE_CLOSE(3, "微信关闭订单API"),
    WX_WAP_PAY(4, "微信wap支付"),
    WX_MP_PAY_INIT(5, "微信Mp支付初始始化"),
    WX_MP_PAY_JS(6, "微信Mp支付中的JS支付"),
    QQ_NATIVE(7, "QQ扫码统一支付接口"),
    QQ_WAP(8, "QQ Wap支付接口"),
    ALIPAY_NATIVE(9, "支付宝扫码统一支付接口"),
    UNION_NATIVE(10, "银联扫码统一支付接口"),
    ALIPAY_TRADE_QUERY(11, "支付宝查询订单API"),
    ALIPAY_TRADE_CLOSE(12, "支付宝关闭订单API"),
    ALIPAY_WAP(13, "支付宝WAP支付接口"),
    JD_NATIVE(14, "京东支付接口"),
    UNION_NET(15, "银联网关支付接口"),
    UNION_QUICK(16, "银联快捷支付接口"),
    WITHDRAW(17, "代付接口"),
    WITHDRAW_QUERY(18, "代付查询接口");

    private Integer type;
    private String info;

    ApiTypeEnum(Integer type, String info) {
        this.type = type;
        this.info = info;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String getInfo(Integer type) {

        String info = "";

        for (ApiTypeEnum p : ApiTypeEnum.values()) {
            if (p.getType().equals(type)) {
                info = p.getInfo();
                break;
            }
        }

        return info;
    }
}
