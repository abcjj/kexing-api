package com.sxt.commons.typeEnum;

// 对应着数据库中的channel_type表数据
public enum ChannelTypeEnum {
    WX_SCAN((byte) 1, "微信扫码"),
    WX_WAP((byte)6, "微信WAP"),
    WX_MP((byte)4, "微信公众号"),

    QQ_SCAN((byte)5, "QQ扫码"),
    QQ_WAP((byte)7, "QQ WAP"),

    ALIPAY_SCAN((byte)2, "支付宝扫码"),
    ALIPAY_WAP((byte)9, "支付宝WAP"),
    ALIPAY_SERVICE((byte)8, "支付宝服务号"),

    JD_SCAN((byte)10, "京东扫码"),

    UNION_SCAN((byte)3, "银联网关"),
    UNION_NET((byte)11, "银联扫码"),
    UNION_QUICK((byte)12, "银联快捷"),

    WITHDRAW((byte)99, "代付");


    private Byte type;
    private String info;

    ChannelTypeEnum(Byte type, String info) {
        this.type = type;
        this.info = info;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String getInfo(Byte type) {

        String info = "";

        for (ChannelTypeEnum p : ChannelTypeEnum.values()) {
            if (p.getType().equals(type)) {
                info = p.getInfo();
                break;
            }
        }

        return info;
    }

    public static ChannelTypeEnum getByType(Byte type) {
        for (ChannelTypeEnum p : ChannelTypeEnum.values()) {
            if (p.getType().equals(type)) {
                return p;
            }
        }
        return null;
    }
}
