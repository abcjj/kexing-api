package com.sxt.commons.typeEnum;

public enum NotifyLogStateEnum {
    UNNOTIFY((byte) 0, "未发送"),
    UNSUCCESS((byte)1, "已发送，但未成功"),
    SUCCESS((byte)2, "成功"),
    FAIL((byte)3, "成功");

    private Byte type;
    private String info;

    NotifyLogStateEnum(Byte type, String info) {
        this.type = type;
        this.info = info;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String getInfo(Byte type) {

        String info = "";

        for (NotifyLogStateEnum p : NotifyLogStateEnum.values()) {
            if (p.getType().equals(type)) {
                info = p.getInfo();
                break;
            }
        }

        return info;
    }

    public static NotifyLogStateEnum getByType(Byte type) {
        for (NotifyLogStateEnum p : NotifyLogStateEnum.values()) {
            if (p.getType().equals(type)) {
                return p;
            }
        }
        return null;
    }
}
