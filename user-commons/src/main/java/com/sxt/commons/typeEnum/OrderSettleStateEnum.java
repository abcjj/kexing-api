package com.sxt.commons.typeEnum;

/**
 * Creator: Yao
 * Date:    2017/9/25
 * For:
 * Other:
 */
public enum OrderSettleStateEnum {

    NOTSETTLE((byte) 0, "未结算"),
    DONE((byte) 1, "已完结"),
    Exception((byte) 2, "结算异常");

    private Byte type;
    private String info;

    OrderSettleStateEnum(Byte type, String info) {
        this.type = type;
        this.info = info;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String getInfo(Short type) {

        String info = "";

        for (OrderSettleStateEnum p : OrderSettleStateEnum.values()) {
            if (p.getType().equals(type)) {
                info = p.getInfo();
                break;
            }
        }

        return info;
    }

    public static OrderSettleStateEnum getByType(Byte type) {
        for (OrderSettleStateEnum p : OrderSettleStateEnum.values()) {
            if (p.getType().equals(type)) {
                return p;
            }
        }
        return null;
    }
}
