package com.sxt.commons.typeEnum;

/**
 * Creator: Yao
 * Date:    2017/9/25
 * For:
 * Other:
 */
public enum OrderStateEnum {

    NOTPAY((short) 1, "未支付"),
    WAITING((short) 6, "等待支付"),//就是表示请求上游接口成功，等待下游支付
    REQERROR((short) 7, "订单失败"),//就是表示请求上游接口失败，没办法支付的

    CLOSE((short) 2, "订单取消"),
    SUCCESS((short) 3, "已支付"),
    FREEZED((short) 4, "已冻结"),
    PAYERROR((short) 5, "支付失败"),//表示用户支付失败
    TOSUCCESS((short) 88, "置成功");

    private Short type;
    private String info;

    OrderStateEnum(Short type, String info) {
        this.type = type;
        this.info = info;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String getInfo(Short type) {

        String info = "";

        for (OrderStateEnum p : OrderStateEnum.values()) {
            if (p.getType().equals(type)) {
                info = p.getInfo();
                break;
            }
        }

        return info;
    }

    public static OrderStateEnum getByType(Short type) {
        for (OrderStateEnum p : OrderStateEnum.values()) {
            if (p.getType().equals(type)) {
                return p;
            }
        }
        return null;
    }
}
