package com.sxt.commons.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Creator: Yao
 * Date:    2017/5/8
 * For:
 * Other:
 */
@Component
public class DateUtils {


    public final static String DATE_PATTERN = "\\d{4}-\\d{1,2}-\\d{1,2}";
    public final static SimpleDateFormat FORMAT_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");
    public final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public final static SimpleDateFormat sdf14 = new SimpleDateFormat("yyyyMMddHHmmss");

    /**
     * 获取添加天数后的日期
     */
    public Date addDays(Date date1, int days) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date1);
        gc.add(5, days);
        return gc.getTime();
    }

    /**
     * 获取当前时间与指定时间的间隔天数
     *
     * @param startDate
     * @return
     */
    public long interval(Date startDate, Date endDate) {

        // check
        Assert.notNull(startDate, "未指定开始时间");
        Assert.notNull(endDate, "未指定结束时间");

        // init
        Calendar now = Calendar.getInstance();
        now.setTime(getStartTime(endDate));
        Calendar start = Calendar.getInstance();
        start.setTime(getStartTime(startDate));

        // calc
        return (now.getTimeInMillis() - start.getTimeInMillis()) / (24 * 60 * 60 * 1000);
    }

    /**
     * 获取指定时间的当天初始时间
     *
     * @param date
     * @return
     */
    private Date getStartTime(Date date) {

        // check
        Assert.notNull(date, "未指定时间");

        // init
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        // update
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    /**
     * 获取指定时间的当天结束时间
     *
     * @param date
     * @return
     */
    private Date getEndTime(Date date) {

        // check
        Assert.notNull(date, "未指定时间");

        // init
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        // update
        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        // 需调整库表时间设置，否则会默认跳到第二天
        // calendar.set(Calendar.MILLISECOND, 999);

        return calendar.getTime();
    }

    public static void main(String[] args) {
        DateUtils dateUtils = new DateUtils();
        Date yesterday = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(yesterday);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        yesterday = calendar.getTime();
        Date now = new Date();
        System.out.println(dateUtils.interval(yesterday, now));
    }


}
