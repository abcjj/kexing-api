package com.sxt.commons.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 * User: rizenguo
 * Date: 2014/10/29
 * Time: 15:23
 */
public class SignMD5Utils {

    public static String getSign(Map<String, Object> map, String secret, String signKey, String secretKey) {
        ArrayList<String> list = new ArrayList<String>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (!"sign".equals(entry.getKey())&&entry.getValue() != "" && !StringUtils.equals(entry.getKey(), signKey)) {
                list.add(entry.getKey() + "=" + entry.getValue() + "&");
            }
        }
        int size = list.size();
        String[] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(arrayToSort[i]);
        }
        String result = sb.toString();
        result += secretKey + "=" + secret;
        //Util.log("Sign Before MD5:" + result);
        result = DigestUtils.md5Hex(result).toUpperCase();
        //Util.log("Sign Result:" + result);
        return result;
    }

    public static String getSign(Map<String, Object> map, String secret, String signKey, String secretKey,boolean isToUpperCase) {
        ArrayList<String> list = new ArrayList<String>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (entry.getValue() != "" && !StringUtils.equals(entry.getKey(), signKey)) {
                list.add(entry.getKey() + "=" + entry.getValue() + "&");
            }
        }
        int size = list.size();
        String[] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(arrayToSort[i]);
        }
        String result = sb.toString();
        result += secretKey + "=" + secret;
        //Util.log("Sign Before MD5:" + result);
        result = DigestUtils.md5Hex(result);
        if(isToUpperCase){
            result = result.toUpperCase();
        }
        //Util.log("Sign Result:" + result);
        return result;
    }

    public static String getSign(Map<String, Object> map, String secret, String signKey) {
        return getSign(map, secret, signKey, "key");
    }

    public static String getSign(Map<String, Object> map, String secret) {
        return getSign(map, secret, null, "key");
    }

    public static String getSignBySeparator(Map<String,Object> params,String separator,String secret){
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String,Object> entry: params.entrySet()) {
            if(entry.getValue() != null){
                sb.append(separator).append(entry.getValue());
            }
        }
        sb.append(separator).append(secret);

        return  DigestUtils.md5Hex(sb.toString());
    }
}
