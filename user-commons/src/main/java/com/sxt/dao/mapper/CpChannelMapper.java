package com.sxt.dao.mapper;


import com.sxt.pojo.entity.CpChannel;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;


@Mapper
public interface CpChannelMapper {

    @Select({
            "select",
            "id, cp_id, channel_type, fee_recharge, rate_advince, state, update_date, create_date",
            "from cp_channel",
            "where cp_id = #{cpId,jdbcType=BIGINT} order by id desc limit #{offset}, #{size} "
    })
    @Results({
            @Result(column="id", property="id", jdbcType= JdbcType.BIGINT, id=true),
            @Result(column="cp_id", property="cpId", jdbcType=JdbcType.BIGINT),
            @Result(column="channel_type", property="channelType", jdbcType=JdbcType.TINYINT),
            @Result(column="fee_recharge", property="feeRecharge", jdbcType=JdbcType.REAL),
            @Result(column="rate_advince", property="rateAdvince", jdbcType=JdbcType.REAL),
            @Result(column="state", property="state", jdbcType=JdbcType.TINYINT),
            @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP)
    })
    public List<CpChannel> selectByCpId(@Param("cpId") long cpId, @Param("offset") int offset, @Param("size") int size);

    @Select({"select count(id) from `cp_channel` where cp_id = #{cpId,jdbcType=BIGINT}"})
    public long countByCpId(@Param("cpId") long cpId);

    @Select({
            "select",
            "id, cp_id, channel_type, fee_recharge, rate_advince, state, update_date, create_date",
            "from cp_channel",
            "where cp_id = #{cpId,jdbcType=BIGINT} and `channel_type`=#{type,jdbcType=TINYINT}" ,
            " order by id desc limit 1 "
    })
    @Results({
            @Result(column="id", property="id", jdbcType= JdbcType.BIGINT, id=true),
            @Result(column="cp_id", property="cpId", jdbcType=JdbcType.BIGINT),
            @Result(column="channel_type", property="channelType", jdbcType=JdbcType.TINYINT),
            @Result(column="fee_recharge", property="feeRecharge", jdbcType=JdbcType.REAL),
            @Result(column="rate_advince", property="rateAdvince", jdbcType=JdbcType.REAL),
            @Result(column="state", property="state", jdbcType=JdbcType.TINYINT),
            @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP)
    })
    public CpChannel selectByCpIdAndType(@Param("cpId") long cpId, @Param("type") byte type);

    @Update({
            "update cp_channel",
            "set `state` = #{state,jdbcType=TINYINT}",
            "where cp_id in(select id from `cp` where agent_id=#{agentId,jdbcType=BIGINT}) and channel_type=#{channelType,jdbcType=TINYINT}"
    })
    public int updateChildState(@Param("agentId") long agentId, @Param("channelType") byte channelType, @Param("state") byte state);
}