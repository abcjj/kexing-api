package com.sxt.dao.mapper;


import com.sxt.pojo.entity.Cp;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

@Mapper
public interface CpMapper {

    @Select({
            "select",
            "id, `type`, app_id, secret_id, agent_id, fee_default_recharge, cost_withdraw, ",
            "fee_withdraw_advince, withdraw_password, notify_url, ip, bind_mobile, create_time, ",
            "account_state, contract_state",
            "from cp",
            "where id = #{id,jdbcType=BIGINT}"
    })
    @Results({
            @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
            @Result(column="type", property="type", jdbcType=JdbcType.TINYINT),
            @Result(column="app_id", property="appId", jdbcType=JdbcType.CHAR),
            @Result(column="secret_id", property="secretId", jdbcType=JdbcType.CHAR),
            @Result(column="agent_id", property="agentId", jdbcType=JdbcType.BIGINT),
            @Result(column="fee_default_recharge", property="feeDefaultRecharge", jdbcType=JdbcType.DECIMAL),
            @Result(column="cost_withdraw", property="costWithdraw", jdbcType=JdbcType.INTEGER),
            @Result(column="fee_withdraw_advince", property="feeWithdrawAdvince", jdbcType=JdbcType.DECIMAL),
            @Result(column="withdraw_password", property="withdrawPassword", jdbcType=JdbcType.VARCHAR),
            @Result(column="notify_url", property="notifyUrl", jdbcType=JdbcType.VARCHAR),
            @Result(column="ip", property="ip", jdbcType=JdbcType.VARCHAR),
            @Result(column="bind_mobile", property="bindMobile", jdbcType=JdbcType.VARCHAR),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="account_state", property="accountState", jdbcType=JdbcType.TINYINT),
            @Result(column="contract_state", property="contractState", jdbcType=JdbcType.TINYINT)
    })
    Cp selectByPrimaryKey(Long id);

}