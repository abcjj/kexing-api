package com.sxt.dao.mapper;

import com.sxt.dao.parent.NotifyLogParentMapper;
import com.sxt.pojo.entity.NotifyLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

@Mapper
public interface NotifyLogMapper extends NotifyLogParentMapper {

    @Select({
            "select",
            "id, notify_url, notify_msg, op_type, `state`, trade_no, attach, rsp, fail_count, ",
            "create_time, last_notify_time",
            "from notify_log",
            "where trade_no = #{tradeNo,jdbcType=CHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.BIGINT, id = true),
            @Result(column = "notify_url", property = "notifyUrl", jdbcType = JdbcType.VARCHAR),
            @Result(column = "notify_msg", property = "notifyMsg", jdbcType = JdbcType.VARCHAR),
            @Result(column = "op_type", property = "opType", jdbcType = JdbcType.TINYINT),
            @Result(column = "state", property = "state", jdbcType = JdbcType.TINYINT),
            @Result(column = "trade_no", property = "tradeNo", jdbcType = JdbcType.CHAR),
            @Result(column = "attach", property = "attach", jdbcType = JdbcType.VARCHAR),
            @Result(column = "rsp", property = "rsp", jdbcType = JdbcType.VARCHAR),
            @Result(column = "fail_count", property = "failCount", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "last_notify_time", property = "lastNotifyTime", jdbcType = JdbcType.TIMESTAMP)
    })
    NotifyLog selectByTradeNo(String tradeNo);

}