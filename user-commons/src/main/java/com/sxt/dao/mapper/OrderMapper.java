
package com.sxt.dao.mapper;

import com.sxt.pojo.entity.Order;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

@Mapper
public interface OrderMapper{
    @Update({
            "update `order`",
            "set cp_id = #{cpId,jdbcType=BIGINT},",
            "body = #{body,jdbcType=VARCHAR},",
            "amount = #{amount,jdbcType=INTEGER},",
            "fee = #{fee,jdbcType=INTEGER},",
            "fee_recharge = #{feeRecharge,jdbcType=DECIMAL},",
            "actual_amount = #{actualAmount,jdbcType=INTEGER},",
            "advance_amount = #{advanceAmount,jdbcType=INTEGER},",
            "disable_amount = #{disableAmount,jdbcType=INTEGER},",
            "agent_id = #{agentId,jdbcType=BIGINT},",
            "agent_fee = #{agentFee,jdbcType=INTEGER},",
            "agent_fee_recharge = #{agentFeeRecharge,jdbcType=DECIMAL},",
            "`state` = #{state,jdbcType=SMALLINT},",
            "trade_no = #{tradeNo,jdbcType=CHAR},",
            "out_trade_no = #{outTradeNo,jdbcType=VARCHAR},",
            "description = #{description,jdbcType=VARCHAR},",
            "sp_trade_no = #{spTradeNo,jdbcType=VARCHAR},",
            "settle_status = #{settleStatus,jdbcType=TINYINT},",
            "remark = #{remark,jdbcType=VARCHAR},",
            "notify_url = #{notifyUrl,jdbcType=VARCHAR},",
            "attach = #{attach,jdbcType=VARCHAR},",
            "sp_id = #{spId,jdbcType=BIGINT},",
            "channel_type = #{channelType,jdbcType=TINYINT},",
            "sp_fee_recharge = #{spFeeRecharge,jdbcType=DECIMAL},",
            "sp_fee = #{spFee,jdbcType=INTEGER},",
            "sp_mch_id = #{spMchId,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "finish_time = #{finishTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Order record);

    @Insert({
            "insert into `order` (cp_id, body, ",
            "amount, fee, fee_recharge, ",
            "actual_amount, advance_amount, ",
            "disable_amount, agent_id, ",
            "agent_fee, agent_fee_recharge, ",
            "`state`, trade_no, out_trade_no, ",
            "description, sp_trade_no, ",
            "settle_status, remark, ",
            "notify_url, attach, ",
            "sp_id, channel_type, ",
            "sp_fee_recharge, sp_fee, ",
            "sp_mch_id, create_time, ",
            "finish_time)",
            "values (#{cpId,jdbcType=BIGINT}, #{body,jdbcType=VARCHAR}, ",
            "#{amount,jdbcType=INTEGER}, #{fee,jdbcType=INTEGER}, #{feeRecharge,jdbcType=DECIMAL}, ",
            "#{actualAmount,jdbcType=INTEGER}, #{advanceAmount,jdbcType=INTEGER}, ",
            "#{disableAmount,jdbcType=INTEGER}, #{agentId,jdbcType=BIGINT}, ",
            "#{agentFee,jdbcType=INTEGER}, #{agentFeeRecharge,jdbcType=DECIMAL}, ",
            "#{state,jdbcType=SMALLINT}, #{tradeNo,jdbcType=CHAR}, #{outTradeNo,jdbcType=VARCHAR}, ",
            "#{description,jdbcType=VARCHAR}, #{spTradeNo,jdbcType=VARCHAR}, ",
            "#{settleStatus,jdbcType=TINYINT}, #{remark,jdbcType=VARCHAR}, ",
            "#{notifyUrl,jdbcType=VARCHAR}, #{attach,jdbcType=VARCHAR}, ",
            "#{spId,jdbcType=BIGINT}, #{channelType,jdbcType=TINYINT}, ",
            "#{spFeeRecharge,jdbcType=DECIMAL}, #{spFee,jdbcType=INTEGER}, ",
            "#{spMchId,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{finishTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insert(Order record);

    @Select({
            "select *",
            "from `order`",
            "where cp_id = #{cp_id,jdbcType=BIGINT}",
            " and out_trade_no = #{out_trade_no,jdbcType=VARCHAR} "
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.BIGINT, id = true),
            @Result(column = "cp_id", property = "cpId", jdbcType = JdbcType.BIGINT),
            @Result(column = "body", property = "body", jdbcType = JdbcType.VARCHAR),
            @Result(column = "amount", property = "amount", jdbcType = JdbcType.INTEGER),
            @Result(column = "fee", property = "fee", jdbcType = JdbcType.INTEGER),
            @Result(column = "fee_recharge", property = "feeRecharge", jdbcType = JdbcType.DECIMAL),
            @Result(column = "actual_amount", property = "actualAmount", jdbcType = JdbcType.INTEGER),
            @Result(column = "advance_amount", property = "advanceAmount", jdbcType = JdbcType.INTEGER),
            @Result(column = "disable_amount", property = "disableAmount", jdbcType = JdbcType.INTEGER),
            @Result(column = "agent_id", property = "agentId", jdbcType = JdbcType.BIGINT),
            @Result(column = "agent_fee", property = "agentFee", jdbcType = JdbcType.INTEGER),
            @Result(column = "agent_fee_recharge", property = "agentFeeRecharge", jdbcType = JdbcType.DECIMAL),
            @Result(column = "state", property = "state", jdbcType = JdbcType.SMALLINT),
            @Result(column = "trade_no", property = "tradeNo", jdbcType = JdbcType.CHAR),
            @Result(column = "out_trade_no", property = "outTradeNo", jdbcType = JdbcType.VARCHAR),
            @Result(column = "description", property = "description", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sp_trade_no", property = "spTradeNo", jdbcType = JdbcType.VARCHAR),
            @Result(column = "settle_status", property = "settleStatus", jdbcType = JdbcType.TINYINT),
            @Result(column = "remark", property = "remark", jdbcType = JdbcType.VARCHAR),
            @Result(column = "notify_url", property = "notifyUrl", jdbcType = JdbcType.VARCHAR),
            @Result(column = "attach", property = "attach", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sp_id", property = "spId", jdbcType = JdbcType.BIGINT),
            @Result(column = "channel_type", property = "channelType", jdbcType = JdbcType.TINYINT),
            @Result(column = "sp_fee_recharge", property = "spFeeRecharge", jdbcType = JdbcType.DECIMAL),
            @Result(column = "sp_fee", property = "spFee", jdbcType = JdbcType.INTEGER),
            @Result(column = "sp_mch_id", property = "spMchId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "finish_time", property = "finishTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Order getByCpidAndOutTradeNo(@Param("cp_id") Long cpid, @Param("out_trade_no") String outTradeNo);

    @Select({
            "select *",
            "from `order`",
            "where trade_no = #{tradeNo,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.BIGINT, id = true),
            @Result(column = "cp_id", property = "cpId", jdbcType = JdbcType.BIGINT),
            @Result(column = "body", property = "body", jdbcType = JdbcType.VARCHAR),
            @Result(column = "amount", property = "amount", jdbcType = JdbcType.INTEGER),
            @Result(column = "fee", property = "fee", jdbcType = JdbcType.INTEGER),
            @Result(column = "fee_recharge", property = "feeRecharge", jdbcType = JdbcType.DECIMAL),
            @Result(column = "actual_amount", property = "actualAmount", jdbcType = JdbcType.INTEGER),
            @Result(column = "advance_amount", property = "advanceAmount", jdbcType = JdbcType.INTEGER),
            @Result(column = "disable_amount", property = "disableAmount", jdbcType = JdbcType.INTEGER),
            @Result(column = "agent_id", property = "agentId", jdbcType = JdbcType.BIGINT),
            @Result(column = "agent_fee", property = "agentFee", jdbcType = JdbcType.INTEGER),
            @Result(column = "agent_fee_recharge", property = "agentFeeRecharge", jdbcType = JdbcType.DECIMAL),
            @Result(column = "state", property = "state", jdbcType = JdbcType.SMALLINT),
            @Result(column = "trade_no", property = "tradeNo", jdbcType = JdbcType.CHAR),
            @Result(column = "out_trade_no", property = "outTradeNo", jdbcType = JdbcType.VARCHAR),
            @Result(column = "description", property = "description", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sp_trade_no", property = "spTradeNo", jdbcType = JdbcType.VARCHAR),
            @Result(column = "settle_status", property = "settleStatus", jdbcType = JdbcType.TINYINT),
            @Result(column = "remark", property = "remark", jdbcType = JdbcType.VARCHAR),
            @Result(column = "notify_url", property = "notifyUrl", jdbcType = JdbcType.VARCHAR),
            @Result(column = "attach", property = "attach", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sp_id", property = "spId", jdbcType = JdbcType.BIGINT),
            @Result(column = "channel_type", property = "channelType", jdbcType = JdbcType.TINYINT),
            @Result(column = "sp_fee_recharge", property = "spFeeRecharge", jdbcType = JdbcType.DECIMAL),
            @Result(column = "sp_fee", property = "spFee", jdbcType = JdbcType.INTEGER),
            @Result(column = "sp_mch_id", property = "spMchId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "finish_time", property = "finishTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Order getByTradeNo(@Param("trade_no") String tradeNo);

}