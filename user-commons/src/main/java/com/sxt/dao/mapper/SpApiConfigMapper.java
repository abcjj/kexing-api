package com.sxt.dao.mapper;

import com.sxt.pojo.entity.SpApiConfig;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

@Mapper
public interface SpApiConfigMapper{
    @Select({
            "select",
            "id, sp_id, channel_type, api_type, `key`, `val`",
            "from sp_api_config",
            "where sp_id = #{spId,jdbcType=BIGINT}",
            "and channel_type = #{channelType,jdbcType=TINYINT}",
            "and api_type = #{apiType,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column="id", property="id", jdbcType= JdbcType.BIGINT, id=true),
            @Result(column="sp_id", property="spId", jdbcType= JdbcType.BIGINT),
            @Result(column="channel_type", property="channelType", jdbcType=JdbcType.TINYINT),
            @Result(column="api_type", property="apiType", jdbcType=JdbcType.INTEGER),
            @Result(column="key", property="key", jdbcType=JdbcType.VARCHAR),
            @Result(column="val", property="val", jdbcType=JdbcType.VARCHAR)
    })
    List<SpApiConfig> getKeyValues(@Param("spId") Long spid,
                                   @Param("channelType") Byte channelTypeEnum,
                                   @Param("apiType") Integer apiTypeEnum);

    @Select({
            "select",
            "id, sp_id, channel_type, api_type, `key`, `val`",
            "from sp_api_config",
            "where sp_id = #{spId,jdbcType=BIGINT}"
    })
    @Results({
            @Result(column="id", property="id", jdbcType= JdbcType.BIGINT, id=true),
            @Result(column="sp_id", property="spId", jdbcType= JdbcType.BIGINT),
            @Result(column="channel_type", property="channelType", jdbcType=JdbcType.TINYINT),
            @Result(column="api_type", property="apiType", jdbcType=JdbcType.INTEGER),
            @Result(column="key", property="key", jdbcType=JdbcType.VARCHAR),
            @Result(column="val", property="val", jdbcType=JdbcType.VARCHAR)
    })
    List<SpApiConfig> selectBySpId(long spId);
}