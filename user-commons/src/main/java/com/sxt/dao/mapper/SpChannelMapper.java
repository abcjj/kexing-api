package com.sxt.dao.mapper;

import com.sxt.pojo.entity.SpChannel;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

@Mapper
public interface SpChannelMapper{

    @Select({
            "select",
            "sc.id, sc.sp_id, sc.channel_type, sc.fee_recharge, sc.state, sc.update_date, ",
            "sc.create_date",
            "from sp_channel sc inner join sp on sc.sp_id=sp.id",
            "where sc.channel_type = #{channelType,jdbcType=INTEGER}",
            "and sc.`state`=#{state,jdbcType=TINYINT} and sp.`state`=0 order by sc.update_date limit 1",
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "sp_id", property = "spId", jdbcType = JdbcType.BIGINT),
            @Result(column = "channel_type", property = "channelType", jdbcType = JdbcType.TINYINT),
            @Result(column = "fee_recharge", property = "feeRecharge", jdbcType = JdbcType.REAL),
            @Result(column = "state", property = "state", jdbcType = JdbcType.TINYINT),
            @Result(column = "update_date", property = "updateDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "create_date", property = "createDate", jdbcType = JdbcType.TIMESTAMP)

    })
    SpChannel selectByChannelTypeAndStatus(@Param("channelType") Byte channelType,
                                           @Param("state") Byte status);

    @Select({
            "SELECT * FROM ((SELECT",
            "sc.id, sc.sp_id, sc.channel_type, sc.fee_recharge, sc.state, sc.update_date, ",
            "sc.create_date",
            "FROM sp_channel sc INNER JOIN sp ON sc.sp_id=sp.id",
            "LEFT JOIN sp_channel_rule scr ON sc.`sp_id`=scr.`sp_id` AND sc.`channel_type`=scr.`channel_type`",
            "WHERE sc.channel_type = #{channelType,jdbcType=INTEGER} AND sc.`state`=0 AND sp.`state`=0 AND sp.delete_flag=0",
            "AND scr.`id` IS NULL",
            "ORDER BY sc.update_date LIMIT 1)",
            "UNION",
            "(SELECT",
            "sc.id, sc.sp_id, sc.channel_type, sc.fee_recharge, sc.state, sc.update_date, ",
            "sc.create_date",
            "FROM sp_channel sc INNER JOIN sp ON sc.sp_id=sp.id",
            "LEFT JOIN sp_channel_rule scr ON sc.`sp_id`=scr.`sp_id` AND sc.`channel_type`=scr.`channel_type`",
            "WHERE sc.channel_type = #{channelType,jdbcType=INTEGER} AND sc.`state`=0 AND sp.`state`=0 ",
            "AND scr.`current_total_amount` >= #{orderAmount,jdbcType=INTEGER} ",
            "AND scr.`min_order_amount` <= #{orderAmount,jdbcType=INTEGER} ",
            "AND scr.`max_order_amount` > #{orderAmount,jdbcType=INTEGER}",
            "AND CASE ADDTIME(scr.`min_time_stop`,scr.`min_time_stop_duration`) > '24:00:00' WHEN 1 ",
            "THEN (CURTIME() BETWEEN scr.`min_time_stop` AND ADDTIME(scr.`min_time_stop`,scr.`min_time_stop_duration`) ",
            "OR ADDTIME(CURTIME(),'24:00:00')BETWEEN scr.`min_time_stop` AND ADDTIME(scr.`min_time_stop`,scr.`min_time_stop_duration`))",
            "ELSE (CURTIME() BETWEEN scr.`min_time_stop` AND ADDTIME(scr.`min_time_stop`,scr.`min_time_stop_duration`)) END",
            "ORDER BY sc.update_date LIMIT 1))T",
            "ORDER BY T.update_date LIMIT 1"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "sp_id", property = "spId", jdbcType = JdbcType.BIGINT),
            @Result(column = "channel_type", property = "channelType", jdbcType = JdbcType.TINYINT),
            @Result(column = "fee_recharge", property = "feeRecharge", jdbcType = JdbcType.REAL),
            @Result(column = "state", property = "state", jdbcType = JdbcType.TINYINT),
            @Result(column = "update_date", property = "updateDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "create_date", property = "createDate", jdbcType = JdbcType.TIMESTAMP)

    })
    SpChannel selectByChannelTypeAndStatusAndRule(@Param("channelType") Byte channelType,
                                                  @Param("orderAmount") Integer orderAmount);



    @Select({
            "select",
            "id, sp_id, channel_type, fee_recharge, state, update_date, ",
            "create_date",
            "from sp_channel"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "sp_id", property = "spId", jdbcType = JdbcType.BIGINT),
            @Result(column = "channel_type", property = "channelType", jdbcType = JdbcType.TINYINT),
            @Result(column = "fee_recharge", property = "feeRecharge", jdbcType = JdbcType.REAL),
            @Result(column = "state", property = "state", jdbcType = JdbcType.TINYINT),
            @Result(column = "update_date", property = "updateDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "create_date", property = "createDate", jdbcType = JdbcType.TIMESTAMP)
    })
    List<SpChannel> all();

    @Select({
            "select",
            "id, sp_id, channel_type, fee_recharge,  state, update_date, ",
            "create_date",
            "from sp_channel",
            "where sp_id=#{spId,jdbcType=BIGINT}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "sp_id", property = "spId", jdbcType = JdbcType.BIGINT),
            @Result(column = "channel_type", property = "channelType", jdbcType = JdbcType.TINYINT),
            @Result(column = "fee_recharge", property = "feeRecharge", jdbcType = JdbcType.REAL),
            @Result(column = "state", property = "state", jdbcType = JdbcType.TINYINT),
            @Result(column = "update_date", property = "updateDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "create_date", property = "createDate", jdbcType = JdbcType.TIMESTAMP)
    })
    List<SpChannel> selectBySpId(@Param("spId") Integer spId);

    @Select({
            "select id, sp_id, channel_type, state, update_date, create_date",
            "from sp_channel",
            "where channel_type = #{channelType,jdbcType=INTEGER} ",
            "and `sp_id`=#{spid,jdbcType=BIGINT} limit 1"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "sp_id", property = "spId", jdbcType = JdbcType.BIGINT),
            @Result(column = "channel_type", property = "channelType", jdbcType = JdbcType.TINYINT),
            @Result(column = "state", property = "state", jdbcType = JdbcType.TINYINT),
            @Result(column = "update_date", property = "updateDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "create_date", property = "createDate", jdbcType = JdbcType.TIMESTAMP)
    })
    SpChannel selectByChannelTypeAndSpid(@Param("channelType") Byte channelType,
                                         @Param("spid") Long sp_id);

    @Update({
            "update sp_channel",
            "set sp_id = #{spId,jdbcType=BIGINT},",
            "channel_type = #{channelType,jdbcType=TINYINT},",
            "fee_recharge = #{feeRecharge,jdbcType=DECIMAL},",
            "`state` = #{state,jdbcType=TINYINT},",
            "update_date = #{updateDate,jdbcType=TIMESTAMP},",
            "create_date = #{createDate,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(SpChannel record);

}