package com.sxt.dao.mapper;

import com.sxt.pojo.entity.SpConfig;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

@Mapper
public interface SpConfigMapper{
    @Select({
            "select",
            "id, sp_id, `key`, `val`",
            "from sp_config",
            "where sp_id = #{spId,jdbcType=BIGINT}"
    })
    @Results({
            @Result(column="id", property="id", jdbcType= JdbcType.BIGINT, id=true),
            @Result(column="sp_id", property="spId", jdbcType= JdbcType.BIGINT),
            @Result(column="key", property="key", jdbcType=JdbcType.VARCHAR),
            @Result(column="val", property="val", jdbcType=JdbcType.VARCHAR)
    })
    List<SpConfig> getKeyValues(@Param("spId") Long spid);

    @Select({
            "select",
            "id, sp_id, `key`, `val`",
            "from sp_config",
            "where sp_id = #{spId,jdbcType=BIGINT} and `key`=#{key,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="id", property="id", jdbcType= JdbcType.BIGINT, id=true),
            @Result(column="sp_id", property="spId", jdbcType= JdbcType.BIGINT),
            @Result(column="key", property="key", jdbcType=JdbcType.VARCHAR),
            @Result(column="val", property="val", jdbcType=JdbcType.VARCHAR)
    })
    SpConfig getValue(@Param("spId") Long spid, @Param("key") String key);
}