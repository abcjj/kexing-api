package com.sxt.dao.mapper;

import com.sxt.pojo.entity.Sp;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

@Mapper
public interface SpMapper{

    @Select({
            "select",
            "id, name, service,bank, update_date, create_date,state,delete_flag,mch_id,card_no,factory_class_name",
            "from sp where id= #{spId,jdbcType=BIGINT}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.BIGINT, id = true),
            @Result(column = "name", property = "name", jdbcType = JdbcType.VARCHAR),
            @Result(column = "service", property = "service", jdbcType = JdbcType.VARCHAR),
            @Result(column = "bank", property = "bank", jdbcType = JdbcType.VARCHAR),
            @Result(column = "update_date", property = "updateDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "create_date", property = "createDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "state", property = "state", jdbcType = JdbcType.TINYINT),
            @Result(column = "delete_flag", property = "deleteFlag", jdbcType = JdbcType.TINYINT),
            @Result(column = "card_no", property = "cardNo", jdbcType = JdbcType.VARCHAR),
            @Result(column = "mch_id", property = "mchId", jdbcType = JdbcType.BIGINT),
            @Result(column = "factory_class_name", property = "factoryClassName", jdbcType = JdbcType.VARCHAR)
    })
    Sp selectById(@Param("spId") Long spId);




}