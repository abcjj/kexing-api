package com.sxt.dao.parent;


import com.sxt.dao.provider.NotifyLogSqlProvider;
import com.sxt.pojo.entity.NotifyLog;
import com.sxt.pojo.model.NotifyLogExample;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

public interface NotifyLogParentMapper {
    @SelectProvider(type= NotifyLogSqlProvider.class, method="countByExample")
    int countByExample(NotifyLogExample example);

    @Delete({
        "delete from notify_log",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into notify_log (notify_url, notify_msg, ",
        "op_type, `state`, trade_no, ",
        "attach, rsp, fail_count, ",
        "create_time, last_notify_time)",
        "values (#{notifyUrl,jdbcType=VARCHAR}, #{notifyMsg,jdbcType=VARCHAR}, ",
        "#{opType,jdbcType=TINYINT}, #{state,jdbcType=TINYINT}, #{tradeNo,jdbcType=CHAR}, ",
        "#{attach,jdbcType=VARCHAR}, #{rsp,jdbcType=VARCHAR}, #{failCount,jdbcType=TINYINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{lastNotifyTime,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insert(NotifyLog record);

    @InsertProvider(type=NotifyLogSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insertSelective(NotifyLog record);

    @SelectProvider(type=NotifyLogSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="notify_url", property="notifyUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="notify_msg", property="notifyMsg", jdbcType=JdbcType.VARCHAR),
        @Result(column="op_type", property="opType", jdbcType=JdbcType.TINYINT),
        @Result(column="state", property="state", jdbcType=JdbcType.TINYINT),
        @Result(column="trade_no", property="tradeNo", jdbcType=JdbcType.CHAR),
        @Result(column="attach", property="attach", jdbcType=JdbcType.VARCHAR),
        @Result(column="rsp", property="rsp", jdbcType=JdbcType.VARCHAR),
        @Result(column="fail_count", property="failCount", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="last_notify_time", property="lastNotifyTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<NotifyLog> selectByExample(NotifyLogExample example);

    @Select({
        "select",
        "id, notify_url, notify_msg, op_type, `state`, trade_no, attach, rsp, fail_count, ",
        "create_time, last_notify_time",
        "from notify_log",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="notify_url", property="notifyUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="notify_msg", property="notifyMsg", jdbcType=JdbcType.VARCHAR),
        @Result(column="op_type", property="opType", jdbcType=JdbcType.TINYINT),
        @Result(column="state", property="state", jdbcType=JdbcType.TINYINT),
        @Result(column="trade_no", property="tradeNo", jdbcType=JdbcType.CHAR),
        @Result(column="attach", property="attach", jdbcType=JdbcType.VARCHAR),
        @Result(column="rsp", property="rsp", jdbcType=JdbcType.VARCHAR),
        @Result(column="fail_count", property="failCount", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="last_notify_time", property="lastNotifyTime", jdbcType=JdbcType.TIMESTAMP)
    })
    NotifyLog selectByPrimaryKey(Long id);

    @UpdateProvider(type=NotifyLogSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(NotifyLog record);

    @Update({
        "update notify_log",
        "set notify_url = #{notifyUrl,jdbcType=VARCHAR},",
          "notify_msg = #{notifyMsg,jdbcType=VARCHAR},",
          "op_type = #{opType,jdbcType=TINYINT},",
          "`state` = #{state,jdbcType=TINYINT},",
          "trade_no = #{tradeNo,jdbcType=CHAR},",
          "attach = #{attach,jdbcType=VARCHAR},",
          "rsp = #{rsp,jdbcType=VARCHAR},",
          "fail_count = #{failCount,jdbcType=TINYINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "last_notify_time = #{lastNotifyTime,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(NotifyLog record);
}