package com.sxt.dao.provider;

import com.sxt.pojo.entity.NotifyLog;
import com.sxt.pojo.model.NotifyLogExample;

import java.util.List;

import static org.apache.ibatis.jdbc.SqlBuilder.*;

public class NotifyLogSqlProvider {

    public String countByExample(NotifyLogExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("notify_log");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(NotifyLog record) {
        BEGIN();
        INSERT_INTO("notify_log");
        
        if (record.getNotifyUrl() != null) {
            VALUES("notify_url", "#{notifyUrl,jdbcType=VARCHAR}");
        }
        
        if (record.getNotifyMsg() != null) {
            VALUES("notify_msg", "#{notifyMsg,jdbcType=VARCHAR}");
        }
        
        if (record.getOpType() != null) {
            VALUES("op_type", "#{opType,jdbcType=TINYINT}");
        }
        
        if (record.getState() != null) {
            VALUES("`state`", "#{state,jdbcType=TINYINT}");
        }
        
        if (record.getTradeNo() != null) {
            VALUES("trade_no", "#{tradeNo,jdbcType=CHAR}");
        }
        
        if (record.getAttach() != null) {
            VALUES("attach", "#{attach,jdbcType=VARCHAR}");
        }
        
        if (record.getRsp() != null) {
            VALUES("rsp", "#{rsp,jdbcType=VARCHAR}");
        }
        
        if (record.getFailCount() != null) {
            VALUES("fail_count", "#{failCount,jdbcType=TINYINT}");
        }
        
        if (record.getCreateTime() != null) {
            VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getLastNotifyTime() != null) {
            VALUES("last_notify_time", "#{lastNotifyTime,jdbcType=TIMESTAMP}");
        }
        
        return SQL();
    }

    public String selectByExample(NotifyLogExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("id");
        } else {
            SELECT("id");
        }
        SELECT("notify_url");
        SELECT("notify_msg");
        SELECT("op_type");
        SELECT("`state`");
        SELECT("trade_no");
        SELECT("attach");
        SELECT("rsp");
        SELECT("fail_count");
        SELECT("create_time");
        SELECT("last_notify_time");
        FROM("notify_log");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        String tmp = "";
        if (example != null && example.getLimit() != null) {
            tmp = " limit " + example.getLimit().toString();
            if (example.getOffset() != null) {
                tmp = tmp + " offset " + example.getOffset().toString();
            }
        }
        return SQL() + tmp;
    }

    public String updateByPrimaryKeySelective(NotifyLog record) {
        BEGIN();
        UPDATE("notify_log");
        
        if (record.getNotifyUrl() != null) {
            SET("notify_url = #{notifyUrl,jdbcType=VARCHAR}");
        }
        
        if (record.getNotifyMsg() != null) {
            SET("notify_msg = #{notifyMsg,jdbcType=VARCHAR}");
        }
        
        if (record.getOpType() != null) {
            SET("op_type = #{opType,jdbcType=TINYINT}");
        }
        
        if (record.getState() != null) {
            SET("`state` = #{state,jdbcType=TINYINT}");
        }
        
        if (record.getTradeNo() != null) {
            SET("trade_no = #{tradeNo,jdbcType=CHAR}");
        }
        
        if (record.getAttach() != null) {
            SET("attach = #{attach,jdbcType=VARCHAR}");
        }
        
        if (record.getRsp() != null) {
            SET("rsp = #{rsp,jdbcType=VARCHAR}");
        }
        
        if (record.getFailCount() != null) {
            SET("fail_count = #{failCount,jdbcType=TINYINT}");
        }
        
        if (record.getCreateTime() != null) {
            SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getLastNotifyTime() != null) {
            SET("last_notify_time = #{lastNotifyTime,jdbcType=TIMESTAMP}");
        }
        
        WHERE("id = #{id,jdbcType=BIGINT}");
        
        return SQL();
    }

    protected void applyWhere(NotifyLogExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<NotifyLogExample.Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            NotifyLogExample.Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<NotifyLogExample.Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    NotifyLogExample.Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}