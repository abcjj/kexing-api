package com.sxt.dao.provider;


import com.sxt.pojo.entity.Sp;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.apache.ibatis.jdbc.SqlBuilder.*;

public class SpSqlProvider{

    public String selectByCondition(Map<String, Object> params) {
        if (params != null && params.size() > 2) {
            List<String> lst = buildAgentIdSql(params);
            String sql = " select * from `sp` where sp.delete_flag ='0' and " + StringUtils.join(lst.toArray(), " and ");
            sql += " ORDER BY `id` desc LIMIT #{offset},#{size}";
            return sql;
        }
        return "SELECT * FROM `sp` where delete_flag='0'  ORDER BY `id` desc LIMIT #{offset},#{size}";

    }

    public String countByCondition(Map<String, Object> params) {
        if (params != null && params.size() > 0) {
            List<String> lst = buildAgentIdSql(params);
            String sql = " select count(0) from `sp` where  sp.delete_flag = '0' and " + StringUtils.join(lst.toArray(), " and ");
            return sql;

        }
        return "SELECT count(0) FROM `sp`  where  sp.delete_flag = '0'  LIMIT 1";
    }

    List<String> buildAgentIdSql(Map<String, Object> params) {
        List<String> lst = new ArrayList<>(4);
        if (params.getOrDefault("name", null) != null) {
            lst.add("`name` like concat(#{name},'%')");
        }
        if (params.getOrDefault("service", null) != null) {
            lst.add("`service` like concat(#{service},'%')");
        }
        if (params.getOrDefault("bank", null) != null) {
            lst.add("`bank` like concat(#{bank},'%')");
        }
        if (params.getOrDefault("state", null) != null) {
            lst.add("`state`=#{state}");
        }
        if (params.getOrDefault("sp_user_name", null) != null) {
            lst.add("`sp_user_name`=#{sp_user_name}");
        }
        return lst;
    }

    public String findByIdIn(List<Long> list, String key) {

        return String.format("select * from `sp` where id in (%s)", StringUtils.join(list, ","));
    }

    public String updateState(Sp record) {
        BEGIN();
        UPDATE("sp");

        if (record.getMchId() != null) {
            SET("mch_id = #{mchId,jdbcType=VARCHAR}");
        }

        if (record.getCardNo() != null) {
            SET("card_no = #{cardNo,jdbcType=VARCHAR}");
        }

        if (record.getDeleteFlag() != null) {
            SET("delete_flag = #{deleteFlag,jdbcType=TINYINT}");
        }

        if (record.getName() != null) {
            SET("`name` = #{name,jdbcType=VARCHAR}");
        }

        if (record.getService() != null) {
            SET("service = #{service,jdbcType=VARCHAR}");
        }

        if (record.getBank() != null) {
            SET("bank = #{bank,jdbcType=VARCHAR}");
        }

        if (record.getFactoryClassName() != null) {
            SET("factory_class_name = #{factoryClassName,jdbcType=VARCHAR}");
        }

        if (record.getUpdateDate() != null) {
            SET("update_date = #{updateDate,jdbcType=TIMESTAMP}");
        }

        if (record.getCreateDate() != null) {
            SET("create_date = #{createDate,jdbcType=TIMESTAMP}");
        }

        if (record.getState() != null) {
            SET("`state` = #{state,jdbcType=TINYINT}");
        }

        if (record.getFailReason() != null) {
            SET("fail_reason = #{failReason,jdbcType=VARCHAR}");
        }else{
            SET("fail_reason = null");
        }

        if (record.getSpUserName() != null) {
            SET("sp_user_name = #{spUserName,jdbcType=VARCHAR}");
        }

        if (record.getZfbNo() != null) {
            SET("zfb_no = #{zfbNo,jdbcType=VARCHAR}");
        }

        WHERE("id = #{id,jdbcType=BIGINT}");

        return SQL();
    }
}