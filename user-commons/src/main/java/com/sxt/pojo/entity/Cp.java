package com.sxt.pojo.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Cp {
    private Long id;

    private Byte type;

    private String appId;

    private String secretId;

    private Long agentId;

    private BigDecimal feeDefaultRecharge;

    private Integer costWithdraw;

    private BigDecimal feeWithdrawAdvince;

    private String withdrawPassword;

    private String notifyUrl;

    private String ip;

    private String bindMobile;

    private Date createTime;

    private Byte accountState;

    private Byte contractState;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId == null ? null : appId.trim();
    }

    public String getSecretId() {
        return secretId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId == null ? null : secretId.trim();
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public BigDecimal getFeeDefaultRecharge() {
        return feeDefaultRecharge;
    }

    public void setFeeDefaultRecharge(BigDecimal feeDefaultRecharge) {
        this.feeDefaultRecharge = feeDefaultRecharge;
    }

    public Integer getCostWithdraw() {
        return costWithdraw;
    }

    public void setCostWithdraw(Integer costWithdraw) {
        this.costWithdraw = costWithdraw;
    }

    public BigDecimal getFeeWithdrawAdvince() {
        return feeWithdrawAdvince;
    }

    public void setFeeWithdrawAdvince(BigDecimal feeWithdrawAdvince) {
        this.feeWithdrawAdvince = feeWithdrawAdvince;
    }

    public String getWithdrawPassword() {
        return withdrawPassword;
    }

    public void setWithdrawPassword(String withdrawPassword) {
        this.withdrawPassword = withdrawPassword == null ? null : withdrawPassword.trim();
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl == null ? null : notifyUrl.trim();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public String getBindMobile() {
        return bindMobile;
    }

    public void setBindMobile(String bindMobile) {
        this.bindMobile = bindMobile == null ? null : bindMobile.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Byte getAccountState() {
        return accountState;
    }

    public void setAccountState(Byte accountState) {
        this.accountState = accountState;
    }

    public Byte getContractState() {
        return contractState;
    }

    public void setContractState(Byte contractState) {
        this.contractState = contractState;
    }
}