package com.sxt.pojo.entity;

public class CpAccount {

    private String name;

    private Long agentId;

    private Long total;//商户实际赢得总金额

    private Long fee;//商户总费用

    private Long agentFee;//商户总费用

    private Long withdrawFee;//提现总费用

    private Long widthdrawAmount;//提现金额

    private Byte accountState;//商户状态


    public Long getWithdrawFee() {
        return withdrawFee;
    }

    public void setWithdrawFee(Long withdrawFee) {
        this.withdrawFee = withdrawFee;
    }

    public Long getAgentFee() {
        return agentFee;
    }

    public void setAgentFee(Long agentFee) {
        this.agentFee = agentFee;
    }

    public Byte getAccountState() {
        return accountState;
    }

    public void setAccountState(Byte accountState) {
        this.accountState = accountState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getFee() {
        return fee;
    }

    public void setFee(Long fee) {
        this.fee = fee;
    }

    public Long getWidthdrawAmount() {
        return widthdrawAmount;
    }

    public void setWidthdrawAmount(Long widthdrawAmount) {
        this.widthdrawAmount = widthdrawAmount;
    }

    //*******************************2018-11-3商户余额改之前的
    private Long cpId;

    private Long balance;

    private Long balanceAvailable;

    private Long balanceAdvance;

    private Long balanceDisabled;

    private Long balanceAdvanceWidthdraw;

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Long getBalanceAvailable() {
        return balanceAvailable;
    }

    public void setBalanceAvailable(Long balanceAvailable) {
        this.balanceAvailable = balanceAvailable;
    }

    public Long getBalanceAdvance() {
        return balanceAdvance;
    }

    public void setBalanceAdvance(Long balanceAdvance) {
        this.balanceAdvance = balanceAdvance;
    }

    public Long getBalanceDisabled() {
        return balanceDisabled;
    }

    public void setBalanceDisabled(Long balanceDisabled) {
        this.balanceDisabled = balanceDisabled;
    }

    public Long getBalanceAdvanceWidthdraw() {
        return balanceAdvanceWidthdraw;
    }

    public void setBalanceAdvanceWidthdraw(Long balanceAdvanceWidthdraw) {
        this.balanceAdvanceWidthdraw = balanceAdvanceWidthdraw;
    }
}