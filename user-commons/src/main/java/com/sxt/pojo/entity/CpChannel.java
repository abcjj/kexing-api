package com.sxt.pojo.entity;

import java.math.BigDecimal;
import java.util.Date;

public class CpChannel {
    private Long id;

    private Long cpId;

    private Byte channelType;

    private BigDecimal feeRecharge;

    private BigDecimal rateAdvince;

    private Byte state;

    private Date updateDate;

    private Date createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public Byte getChannelType() {
        return channelType;
    }

    public void setChannelType(Byte channelType) {
        this.channelType = channelType;
    }

    public BigDecimal getFeeRecharge() {
        return feeRecharge;
    }

    public void setFeeRecharge(BigDecimal feeRecharge) {
        this.feeRecharge = feeRecharge;
    }

    public BigDecimal getRateAdvince() {
        return rateAdvince;
    }

    public void setRateAdvince(BigDecimal rateAdvince) {
        this.rateAdvince = rateAdvince;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}