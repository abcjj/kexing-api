package com.sxt.pojo.entity;

import java.math.BigDecimal;
import java.util.Date;

public class OrderSettleDetail {
    private Long id;

    private Long cpId;

    private String body;

    private Integer amount;

    private Integer fee;

    private BigDecimal feeRecharge;

    private Integer actualAmount;

    private Integer advanceAmount;

    private Integer disableAmount;

    private Long agentId;

    private Integer agentFee;

    private BigDecimal agentFeeRecharge;

    private Short state;

    private String tradeNo;

    private String outTradeNo;

    private String description;

    private String spTradeNo;

    private Byte settleStatus;

    private String remark;

    private String notifyUrl;

    private String attach;

    private Long spId;

    private Byte channelType;

    private BigDecimal spFeeRecharge;

    private Integer spFee;

    private String spMchId;

    private Date createTime;

    private Date finishTime;

    private Integer actualPayAmount;

    public Integer getActualPayAmount() {
        return actualPayAmount;
    }

    public void setActualPayAmount(Integer actualPayAmount) {
        this.actualPayAmount = actualPayAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body == null ? null : body.trim();
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getFee() {
        return fee;
    }

    public void setFee(Integer fee) {
        this.fee = fee;
    }

    public BigDecimal getFeeRecharge() {
        return feeRecharge;
    }

    public void setFeeRecharge(BigDecimal feeRecharge) {
        this.feeRecharge = feeRecharge;
    }

    public Integer getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(Integer actualAmount) {
        this.actualAmount = actualAmount;
    }

    public Integer getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(Integer advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public Integer getDisableAmount() {
        return disableAmount;
    }

    public void setDisableAmount(Integer disableAmount) {
        this.disableAmount = disableAmount;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Integer getAgentFee() {
        return agentFee;
    }

    public void setAgentFee(Integer agentFee) {
        this.agentFee = agentFee;
    }

    public BigDecimal getAgentFeeRecharge() {
        return agentFeeRecharge;
    }

    public void setAgentFeeRecharge(BigDecimal agentFeeRecharge) {
        this.agentFeeRecharge = agentFeeRecharge;
    }

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo == null ? null : tradeNo.trim();
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo == null ? null : outTradeNo.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getSpTradeNo() {
        return spTradeNo;
    }

    public void setSpTradeNo(String spTradeNo) {
        this.spTradeNo = spTradeNo == null ? null : spTradeNo.trim();
    }

    public Byte getSettleStatus() {
        return settleStatus;
    }

    public void setSettleStatus(Byte settleStatus) {
        this.settleStatus = settleStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl == null ? null : notifyUrl.trim();
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach == null ? null : attach.trim();
    }

    public Long getSpId() {
        return spId;
    }

    public void setSpId(Long spId) {
        this.spId = spId;
    }

    public Byte getChannelType() {
        return channelType;
    }

    public void setChannelType(Byte channelType) {
        this.channelType = channelType;
    }

    public BigDecimal getSpFeeRecharge() {
        return spFeeRecharge;
    }

    public void setSpFeeRecharge(BigDecimal spFeeRecharge) {
        this.spFeeRecharge = spFeeRecharge;
    }

    public Integer getSpFee() {
        return spFee;
    }

    public void setSpFee(Integer spFee) {
        this.spFee = spFee;
    }

    public String getSpMchId() {
        return spMchId;
    }

    public void setSpMchId(String spMchId) {
        this.spMchId = spMchId == null ? null : spMchId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }
}