package com.sxt.pojo.entity;

import java.util.Date;

public class Sp {
    private Long id;

    private String mchId;

    private String cardNo;

    private Byte deleteFlag;

    private String name;

    private String service;

    private String bank;

    private String factoryClassName;

    private Date updateDate;

    private Date createDate;

    private Byte state;

    private String failReason;

    private String spUserName;

    private String zfbNo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId == null ? null : mchId.trim();
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo == null ? null : cardNo.trim();
    }

    public Byte getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Byte deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service == null ? null : service.trim();
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank == null ? null : bank.trim();
    }

    public String getFactoryClassName() {
        return factoryClassName;
    }

    public void setFactoryClassName(String factoryClassName) {
        this.factoryClassName = factoryClassName == null ? null : factoryClassName.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public String getFailReason() {
        return failReason;
    }

    public void setFailReason(String failReason) {
        this.failReason = failReason == null ? null : failReason.trim();
    }

    public String getSpUserName() {
        return spUserName;
    }

    public void setSpUserName(String spUserName) {
        this.spUserName = spUserName == null ? null : spUserName.trim();
    }

    public String getZfbNo() {
        return zfbNo;
    }

    public void setZfbNo(String zfbNo) {
        this.zfbNo = zfbNo == null ? null : zfbNo.trim();
    }
}