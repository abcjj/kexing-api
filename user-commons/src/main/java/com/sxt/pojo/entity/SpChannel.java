package com.sxt.pojo.entity;

import java.math.BigDecimal;
import java.util.Date;

public class SpChannel {
    private Integer id;

    private Long spId;

    private Byte channelType;

    private BigDecimal feeRecharge;

    private Byte state;

    private Date updateDate;

    private Date createDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getSpId() {
        return spId;
    }

    public void setSpId(Long spId) {
        this.spId = spId;
    }

    public Byte getChannelType() {
        return channelType;
    }

    public void setChannelType(Byte channelType) {
        this.channelType = channelType;
    }

    public BigDecimal getFeeRecharge() {
        return feeRecharge;
    }

    public void setFeeRecharge(BigDecimal feeRecharge) {
        this.feeRecharge = feeRecharge;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}