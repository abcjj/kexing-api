package com.sxt.pojo.entity;

import java.util.Date;

public class SpChannelRule {
    private Long id;

    private Long spId;

    private Byte channelType;

    private Long maxTotalAmount;

    private Long currentTotalAmount;

    private Date totalAmountLastTime;

    private Long maxOrderAmount;

    private Long minOrderAmount;

    private Date minTimeStop;

    private Date minTimeStopDuration;

    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpId() {
        return spId;
    }

    public void setSpId(Long spId) {
        this.spId = spId;
    }

    public Byte getChannelType() {
        return channelType;
    }

    public void setChannelType(Byte channelType) {
        this.channelType = channelType;
    }

    public Long getMaxTotalAmount() {
        return maxTotalAmount;
    }

    public void setMaxTotalAmount(Long maxTotalAmount) {
        this.maxTotalAmount = maxTotalAmount;
    }

    public Long getCurrentTotalAmount() {
        return currentTotalAmount;
    }

    public void setCurrentTotalAmount(Long currentTotalAmount) {
        this.currentTotalAmount = currentTotalAmount;
    }

    public Date getTotalAmountLastTime() {
        return totalAmountLastTime;
    }

    public void setTotalAmountLastTime(Date totalAmountLastTime) {
        this.totalAmountLastTime = totalAmountLastTime;
    }

    public Long getMaxOrderAmount() {
        return maxOrderAmount;
    }

    public void setMaxOrderAmount(Long maxOrderAmount) {
        this.maxOrderAmount = maxOrderAmount;
    }

    public Long getMinOrderAmount() {
        return minOrderAmount;
    }

    public void setMinOrderAmount(Long minOrderAmount) {
        this.minOrderAmount = minOrderAmount;
    }

    public Date getMinTimeStop() {
        return minTimeStop;
    }

    public void setMinTimeStop(Date minTimeStop) {
        this.minTimeStop = minTimeStop;
    }

    public Date getMinTimeStopDuration() {
        return minTimeStopDuration;
    }

    public void setMinTimeStopDuration(Date minTimeStopDuration) {
        this.minTimeStopDuration = minTimeStopDuration;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}