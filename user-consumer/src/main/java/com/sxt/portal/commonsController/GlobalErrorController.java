package com.sxt.portal.commonsController;

import com.sxt.commons.dto.ApiCreateOrderResponse;
import com.sxt.commons.typeEnum.ApiStatusEnum;
import com.sxt.commons.utils.ResultUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * controller统一异常处理
 */
@ControllerAdvice
public class GlobalErrorController {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ApiCreateOrderResponse jsonErrorHandler(HttpServletRequest req,
                                                   Exception e) throws Exception {
        Map<String, Object> result =
                ResultUtils.createStatusResult(ApiStatusEnum.SERVICE_ERR.getCode().toString(),
                        ApiStatusEnum.SERVICE_ERR.getInfo());
        logger.error("API请求出错", e);
        return ApiCreateOrderResponse.error(result);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class, IllegalStateException.class})
    @ResponseBody
    public ApiCreateOrderResponse AssertErrorHandler(HttpServletRequest req,
                                     Exception e) throws Exception {
        String errorMsg = e.getMessage();
        ApiStatusEnum apiStatusEnum = ResultUtils.getApiType(errorMsg, ApiStatusEnum.ARGUMENT_REQUEST);
        errorMsg = ResultUtils.getErrorMsg(errorMsg);
        Map<String, Object> result =
                ResultUtils.createStatusResult(apiStatusEnum.getCode().toString(),
                        errorMsg);

        return ApiCreateOrderResponse.error(result);
    }

}
