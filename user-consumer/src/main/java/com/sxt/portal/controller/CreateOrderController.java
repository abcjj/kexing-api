package com.sxt.portal.controller;

import com.sxt.commons.dto.ApiCreateOrderRequest;
import com.sxt.commons.dto.ApiCreateOrderResponse;
import com.sxt.commons.typeEnum.ApiStatusEnum;
import com.sxt.commons.utils.ResultUtils;
import com.sxt.portal.interceptor.SignInterceptor;
import com.sxt.portal.service.CreateOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/pay")
public class CreateOrderController {
	Logger logger = LoggerFactory.getLogger(SignInterceptor.class);
	@Autowired
	private CreateOrderService createOrderService;

	@RequestMapping("/createOrder")
	public ApiCreateOrderResponse createOrder(ApiCreateOrderRequest request) throws Exception {
		logger.info("request body: "+request.toString());
		Map<String,Object> result = this.createOrderService.createOrder(request);
		Assert.notNull(result,
				ResultUtils.buildErrorMsg(ApiStatusEnum.SERVICE_BUSY, ApiStatusEnum.SERVICE_BUSY.getInfo()));

		return ApiCreateOrderResponse.ok(result);
	}
}
