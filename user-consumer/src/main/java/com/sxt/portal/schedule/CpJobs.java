package com.sxt.portal.schedule;

import com.sxt.portal.service.JobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@EnableScheduling
@Component
public class CpJobs {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    public final static long ONE_Minute = 60 * 1000;
    public final static long ONE_Second = 1000;

    @Value("${schedule.open}")
    protected Boolean scheduleOpen;
    @Autowired
    JobService jobService;



    /**通知*/
    @Scheduled(fixedDelay = ONE_Second * 1)
    public void notifyCp() {
        if(scheduleOpen)jobService.sendNotify();
    }
    /**金额的跳转*/
//    @Scheduled(cron = "0 0 1 * * *")
//    public void account() {
//        if(scheduleOpen){
//            int size = 10;
//            int page = 1;
//            do {
//                List<Order> orders = orderService.getOrderFinishTimeLargeThenOneDay(size, page);
//                if (orders != null && orders.size() > 0) {
//                    orders.stream().forEach(p -> {
//                        try {
//                            //logger.info("set order done:" + p.getTradeNo());
//                            cpAccountService.setOrderDone(p);
//                            p.setSettleStatus(OrderSettleStateEnum.DONE.getType());
//                            orderService.update(p);
//                        } catch (Exception e) {
//                            logger.error("set order done:" + p.getTradeNo());
//                            logger.error(e.getMessage());
//                            e.printStackTrace();
//                        }
//                    });
//                } else {
//                    break;
//                }
//            }while (true);
//        }
//
//    }

    /**sp管理的额度*/
//    @Scheduled(cron = "0 0 0 * * *")
//    public void resetSpChannelRule() {
//        if(scheduleOpen){
//            spChannelRuleService.resetCurrentTotalAmount();
//        }
//    }
}
