package com.sxt.portal.service;

import com.sxt.commons.dto.ApiCreateOrderRequest;

import java.util.Map;

/**
 * 专门为Consumer中的商户下单提供的服务接口。
 */
public interface CreateOrderService {

	Map<String, Object> createOrder(ApiCreateOrderRequest request) throws Exception;

	String createSign(Map<String, Object> params, String mchId) throws Exception;

}
