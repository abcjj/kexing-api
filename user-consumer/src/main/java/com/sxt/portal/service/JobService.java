package com.sxt.portal.service;

/**
 * 专门为Consumer中的商户下单提供的服务接口。
 */
public interface JobService {

	/**
	 * 通知
	 */
	void sendNotify();

}
