package com.sxt.portal.service;

import com.sxt.commons.dto.ApiQueryOrderRequest;

import java.util.Map;

/**
 * 专门为Consumer中的商户下单提供的服务接口。
 */
public interface QueryOrderService {

	Map<String, Object> queryOrder(ApiQueryOrderRequest request) throws Exception;


}
