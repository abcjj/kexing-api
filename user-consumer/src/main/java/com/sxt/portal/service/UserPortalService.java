package com.sxt.portal.service;

import com.sxt.commons.dto.UserProviderResponse;
import com.sxt.pojo.entity.User;

/**
 * 专门为Consumer中的UI门户系统提供的服务接口。
 */
public interface UserPortalService {

	UserProviderResponse addUser(User user);

	UserProviderResponse modifyUser(User user);

	UserProviderResponse getUserById(Long id);

	UserProviderResponse deleteUser(Long[] ids);

	UserProviderResponse listUsers(int page, int rows);

}
