package com.sxt.portal.service.impl;

import com.sxt.commons.dto.CallbackResponse;
import com.sxt.portal.service.CallbackService;
import com.sxt.provider.merchant.CallbackProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CallbackServiceImpl implements CallbackService {
	/**
	 * 引用远程的服务提供者。
	 * 就是一次RPC的调用过程。
	 */
	@Autowired
	private CallbackProviderService callbackProviderService;

	@Override
	public CallbackResponse fuhuiCallback(Map<String, Object> params) throws Exception {
		return callbackProviderService.fuhuiCallback(params);
	}


}
