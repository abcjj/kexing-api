package com.sxt.portal.service.impl;

import com.sxt.commons.dto.ApiCreateOrderRequest;
import com.sxt.portal.service.CreateOrderService;
import com.sxt.provider.merchant.CreateOrderProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CreateOrderServiceImpl implements CreateOrderService {

	/**
	 * 引用远程的服务提供者。
	 * 就是一次RPC的调用过程。
	 */
	@Autowired
	private CreateOrderProviderService createOrderProviderService;

	@Override
	public Map<String, Object> createOrder(ApiCreateOrderRequest request) throws Exception{
		Map<String, Object> result = createOrderProviderService.createOrder(request);
		return result;
	}

	@Override
	public String createSign(Map<String, Object> params, String mchId) throws Exception {
		return createOrderProviderService.createSign(params,mchId);
	}
}
