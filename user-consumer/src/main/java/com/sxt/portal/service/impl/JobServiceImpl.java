package com.sxt.portal.service.impl;

import com.sxt.portal.service.JobService;
import com.sxt.provider.job.JobProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JobServiceImpl implements JobService {

	/**
	 * 引用远程的服务提供者。
	 * 就是一次RPC的调用过程。
	 */
	@Autowired
	private JobProviderService jobProviderService;


	@Override
	public void sendNotify(){
		jobProviderService.sendNotify();
	}
}
