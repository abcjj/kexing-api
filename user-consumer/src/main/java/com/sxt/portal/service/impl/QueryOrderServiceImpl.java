package com.sxt.portal.service.impl;

import com.sxt.commons.dto.ApiQueryOrderRequest;
import com.sxt.portal.service.QueryOrderService;
import com.sxt.provider.merchant.QueryOrderProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class QueryOrderServiceImpl implements QueryOrderService {

	/**
	 * 引用远程的服务提供者。
	 * 就是一次RPC的调用过程。
	 */
	@Autowired
	private QueryOrderProviderService queryOrderProviderService;

	@Override
	public Map<String, Object> queryOrder(ApiQueryOrderRequest request) throws Exception {
		return queryOrderProviderService.queryOrder(request);
	}
}
