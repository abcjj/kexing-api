package com.sxt.portal.service.impl;

import com.sxt.commons.dto.UserProviderResponse;
import com.sxt.pojo.entity.User;
import com.sxt.portal.service.UserPortalService;
import com.sxt.provider.user.UserProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserPortalServiceImpl implements UserPortalService {

	/**
	 * 引用远程的服务提供者。
	 * 就是一次RPC的调用过程。
	 */
	@Autowired
	private UserProviderService userProvider;

	@Override
	public UserProviderResponse addUser(User user) {
		UserProviderResponse response = this.userProvider.saveUser(user);
		return response;
	}

	@Override
	public UserProviderResponse modifyUser(User user) {
		UserProviderResponse response = this.userProvider.modifyUser(user);
		return response;
	}

	@Override
	public UserProviderResponse getUserById(Long id) {
		UserProviderResponse response = this.userProvider.findUserById(id);
		return response;
	}

	@Override
	public UserProviderResponse deleteUser(Long[] ids) {
		List<Long> idList = new ArrayList<Long>();
		for(Long id : ids){
			idList.add(id);
		}
		UserProviderResponse response = this.userProvider.removeUsersById(idList);
		return response;
	}

	@Override
	public UserProviderResponse listUsers(int page, int rows) {

		UserProviderResponse response = this.userProvider.findUserByPage(page, rows);

		return response;
	}

}
