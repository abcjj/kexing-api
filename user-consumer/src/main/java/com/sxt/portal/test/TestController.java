package com.sxt.portal.test;

import com.sxt.commons.dto.CallbackResponse;
import com.sxt.commons.utils.RequestUtils;

import com.sxt.portal.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 接收回调，通知
 */
@RestController
@RequestMapping("/callback")
public class TestController {

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    TestService testService;
    /**
     * 付汇
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/test")
    private String fuhui(
            HttpServletRequest request,HttpServletResponse response ){
        try {
            Map<String, Object> params = RequestUtils.getRequestParams(request);
            logger.info("request from " + request.getRequestURI() + ", request body:" + params);
            CallbackResponse response1 = testService.fuhuiCallback(params);
            if(response1.isOk()) return "success";
        }catch (Exception e){
            logger.info("callback error" + request.getRequestURI());
        }
        return "fail";
    }

}
