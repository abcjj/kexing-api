package com.sxt.provider;


import com.alibaba.dubbo.container.Main;

/**
 * 用dubbo相关的main方法来启动
 */
public class ProviderApplicationMain {
    public static void main(String[] args){
        Main.main(args);
    }
}
