package com.sxt.provider.cp;


import com.sxt.pojo.entity.CpChannel;

public interface CpChannelService {

    //根据CpChannel去查询代理商的Channel
    CpChannel getAgentChannel(CpChannel childChannel);
    //根据cpId，type去查询商户的的Channel
    CpChannel getByCpIdAndChannelType(Long cpId, Byte type);


}
