package com.sxt.provider.cp;


import com.sxt.pojo.entity.Cp;

public interface CpService {

  Cp selectById(Long cpId);
}
