package com.sxt.provider.cp.impl;

import com.sxt.dao.mapper.CpChannelMapper;
import com.sxt.pojo.entity.Cp;
import com.sxt.pojo.entity.CpChannel;
import com.sxt.provider.cp.CpChannelService;
import com.sxt.provider.cp.CpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;


@Service
public class CpChannelServiceImpl implements CpChannelService {
    @Autowired
    CpChannelMapper cpChannelMapper;
    @Autowired
    CpService cpService;

    @Override
    public CpChannel getAgentChannel(CpChannel childChannel) {
        if (childChannel == null) {
            return null;
        }

        Cp childCp = cpService.selectById(childChannel.getCpId());
        if (childCp == null || childCp.getAgentId() == 0) {
            return null;
        }

        Long agentId = childCp.getAgentId();
        CpChannel agentChannel = getByCpIdAndChannelType(agentId, childChannel.getChannelType());
        return agentChannel;
    }

    @Override
    public CpChannel getByCpIdAndChannelType(Long cpId, Byte type) {
        return cpChannelMapper.selectByCpIdAndType(cpId, type);
    }


}
