package com.sxt.provider.cp.impl;


import com.sxt.dao.mapper.CpMapper;
import com.sxt.pojo.entity.Cp;
import com.sxt.provider.cp.CpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CpServiceImpl implements CpService {

    @Autowired
    CpMapper cpMapper;


    @Override
    public Cp selectById(Long cpId) {
        return cpMapper.selectByPrimaryKey(cpId);
    }
}
