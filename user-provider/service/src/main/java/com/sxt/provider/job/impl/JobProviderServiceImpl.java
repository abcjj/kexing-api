package com.sxt.provider.job.impl;

import com.sxt.pojo.entity.NotifyLog;
import com.sxt.provider.job.JobProviderService;
import com.sxt.provider.notify.NotifyLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("jobProvider")
public class JobProviderServiceImpl implements JobProviderService {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	NotifyLogService notifyLogService;

	@Override
	public void sendNotify() {
		int size = 20;
		List<NotifyLog> notifyList = notifyLogService.getNotifyList(size);
		if (notifyList != null && notifyList.size() > 0) {
			notifyList.stream().forEach(p -> {
				try {
					logger.info("send notify:" + p.getId());
					notifyLogService.send(p);
				} catch (Exception ex) {
					logger.error("send notify:" + p.getId());
					logger.error(ex.toString());
				}
			});
		}
	}
}
