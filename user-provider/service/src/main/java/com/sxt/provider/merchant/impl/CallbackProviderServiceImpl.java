package com.sxt.provider.merchant.impl;

import com.sxt.commons.dto.CallbackResponse;
import com.sxt.provider.callback.FuhuiCallbackController;
import com.sxt.provider.merchant.CallbackProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("callbackProvider")
public class CallbackProviderServiceImpl implements CallbackProviderService {

	@Autowired
	FuhuiCallbackController fuhuiCallbackController;

	//付汇
	@Override
	public CallbackResponse fuhuiCallback(Map<String, Object> params) throws Exception {
		return fuhuiCallbackController.fuhuiCallback(params);
	}


}
