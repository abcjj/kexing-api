package com.sxt.provider.notify;


import com.sxt.pojo.entity.Cp;
import com.sxt.pojo.entity.NotifyLog;
import com.sxt.pojo.entity.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;


public interface NotifyLogService {

    @Transactional
    List<NotifyLog> getNotifyList(int size);

    @Async
    @Transactional
    void send(NotifyLog notifyLog);

    @Transactional
    void setOrderSuccess(Order order, Cp cp);
}
