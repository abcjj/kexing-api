package com.sxt.provider.order;


import com.sxt.pojo.entity.Order;

/**
 * Creator: Yao
 * Date:    2017/9/21
 * For:
 * Other:
 */
public interface OrderService {

    /**
     * 唯一订单号：uuid
     * @return
     */
    String generateUUid();
    /**
     * 生产订单号
     * @return
     */
    String generateTradeNo();

    /**
     * 轮询选择sp
     * @param order
     * @param type
     * @return
     */
    Order selectASp(Order order, Byte type);

    /**
     * 查询订单
     * @param tradeNo
     * @return
     */
    Order getByTradeNo(String tradeNo);

    /**
     * 设为成功
     * @param tradeNo
     * @return
     */
    Order setOrderSuccess(String tradeNo);

    /**
     * 根据商户号和商户订单查询订单
     * @param cpid
     * @param outTradeNo
     * @return
     */
    Order getByCpidAndOutTradeNo(Long cpid, String outTradeNo);

    /**
     * 新增
     * @param order
     * @return
     */
    Order insert(Order order);

    /**
     * 更新
     * @param order
     * @return
     */
    Order update(Order order);



}
