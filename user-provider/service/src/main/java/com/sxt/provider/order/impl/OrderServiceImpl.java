package com.sxt.provider.order.impl;

import com.sxt.commons.typeEnum.OrderStateEnum;
import com.sxt.dao.mapper.OrderMapper;
import com.sxt.dao.mapper.SpChannelMapper;
import com.sxt.pojo.entity.Cp;
import com.sxt.pojo.entity.Order;
import com.sxt.pojo.entity.SpChannel;
import com.sxt.provider.cp.CpService;
import com.sxt.provider.notify.NotifyLogService;
import com.sxt.provider.order.OrderService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * Creator: fu
 * Date:    2017/9/21
 * For:
 * Other:
 */
@Service
public class OrderServiceImpl implements OrderService {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    SpChannelMapper spChannelMapper;
    @Autowired
    OrderMapper orderMapper;
    @Autowired
    NotifyLogService notifyLogService;
    @Autowired
    CpService cpService;

    @Override
    public String generateUUid() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    @Override
    public String generateTradeNo() {
        return  String.valueOf(System.currentTimeMillis());
    }

    @Override
    public Order selectASp(Order order,Byte type) {
        //轮询,找通道
        SpChannel spChannel = spChannelMapper.selectByChannelTypeAndStatusAndRule
                (type, order.getAmount());
        Assert.notNull(spChannel, "找不到可用的支付通道");
        spChannel.setUpdateDate(new Date());
        spChannelMapper.updateByPrimaryKey(spChannel);

        //组装订单参数
        order.setSpId(spChannel.getSpId());
        order.setSpFeeRecharge(spChannel.getFeeRecharge());
        order.setSpFee(spChannel.getFeeRecharge().multiply(
                new BigDecimal(order.getAmount().toString())
        ).intValue());
        return order;
    }

    @Override
    public Order getByTradeNo(String tradeNo) {
        return orderMapper.getByTradeNo(tradeNo);
    }
    @Override
    public Order setOrderSuccess(String tradeNo) {
        Order order = getByTradeNo(tradeNo);
        Assert.notNull(order, "找不到订单信息");
        if (Objects.equals(order.getState(), OrderStateEnum.SUCCESS.getType())) {
            return order;
        }
        Assert.isTrue(Objects.equals(order.getState(), OrderStateEnum.NOTPAY.getType())||Objects.equals(order.getState(), OrderStateEnum.TOSUCCESS.getType())
                        ||Objects.equals(order.getState(), OrderStateEnum.WAITING.getType()),
                "错误的订单状态，当前订单状态为" + OrderStateEnum.getByType(order.getState()));

        order.setState(OrderStateEnum.SUCCESS.getType());
        order.setFinishTime(new Date());

        Cp cp = cpService.selectById(order.getCpId());
        notifyLogService.setOrderSuccess(order, cp);

        //spChannelRuleService.setOrderSuccess(order);
        update(order);
        return order;
    }



    @Override
    public Order getByCpidAndOutTradeNo(Long cpid, String outTradeNo) {
        return orderMapper.getByCpidAndOutTradeNo(cpid,outTradeNo);
    }

    @Override
    public Order insert(Order order) {
        orderMapper.insert(order);
        return order;
    }

    @Override
    public Order update(Order order) {
        Assert.notNull(order, "订单信息为空");
        orderMapper.updateByPrimaryKey(order);
        return order;
    }
}
