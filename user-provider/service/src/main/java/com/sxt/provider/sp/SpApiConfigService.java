package com.sxt.provider.sp;



import com.sxt.commons.typeEnum.ApiTypeEnum;
import com.sxt.commons.typeEnum.ChannelTypeEnum;
import com.sxt.pojo.entity.SpApiConfig;

import java.util.List;
import java.util.Map;

public interface SpApiConfigService {
    Map<String, Object> getKeyValues(Long spid,
                                     ChannelTypeEnum channelTypeEnum,
                                     ApiTypeEnum apiTypeEnum);

    List<SpApiConfig> all();
    List<SpApiConfig> selectBySpId(int id);
}
