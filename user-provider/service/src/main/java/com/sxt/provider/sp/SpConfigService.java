package com.sxt.provider.sp;

import com.sxt.pojo.entity.SpConfig;

import java.util.List;
import java.util.Map;

public interface SpConfigService {
    Map<String, Object> getKeyValues(Long spid);
    String getValue(Long spid, String key);
    List<SpConfig> getBySpId(Long spid);
}
