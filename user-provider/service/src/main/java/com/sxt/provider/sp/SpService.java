package com.sxt.provider.sp;


import com.sxt.pojo.entity.Sp;
import com.sxt.provider.sp.excutor.ExcutorFactory;

public interface SpService {

    ExcutorFactory getFactory(Long spid)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException;

    ExcutorFactory getFactory(Sp sp)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException;

}
