package com.sxt.provider.sp.excutor;

public interface ExcutorFactory {
    // 微信支付统一下订单
    default AbstractExcutor createWxNativeExcutor() {
        return null;
    };
    // 微信支付交易查询
    default AbstractExcutor createWxTradeQueryExcutor() {
        return null;
    };
    // 微信支付交易关闭
    default AbstractExcutor createWxTradeCloseExcutor() {
        return null;
    };

    // 微信WAP支付
    default AbstractExcutor createWxWapPayExcutor() {
        return null;
    };

    // 微信MP支付
    default AbstractExcutor createWxMpPayExcutor() {
        return null;
    };

    // QQ支付统一下订单
    default AbstractExcutor createQQNativeExcutor() {
        return null;
    };

    // QQ Wap支付订单
    default AbstractExcutor createQQWapExcutor() {
        return null;
    };

    // 支付宝统一下订单
    default AbstractExcutor createAlipayNativeExcutor() {
        return null;
    };
    // 支付宝交易查询
    default AbstractExcutor createAlipayTradeQueryExcutor() {
        return null;
    };
    // 支付宝交易关闭
    default AbstractExcutor createAlipayTradeCloseExcutor() {
        return null;
    };
    // 支付宝WAP支付
    default AbstractExcutor createAlipayWapExcutor() {
        return null;
    };

    // 支付宝单笔转账支付
    default AbstractExcutor createAlipayTransferExcutor() {
        return null;
    };
    // 支付宝结算支付
    default AbstractExcutor createAlipaySettleExcutor() {
        return null;
    };


    // 银联统一下订单
    default AbstractExcutor createUnionNativeExcutor() {
        return null;
    };
    // 银联网关支付
    default AbstractExcutor createUnionNetExcutor() {
        return null;
    };
    // 银联快捷支付开通
    default AbstractExcutor createUnionQuickOpenExcutor() {
        return null;
    };
    // 银联快捷支付
    default AbstractExcutor createUnionQuickExcutor() {
        return null;
    };
    // 银联快捷支付确认
    default AbstractExcutor createUnionQuickConfirmExcutor() {
        return null;
    };

    //京东支付
    default AbstractExcutor createJdNativeExcutor() {
        return null;
    };

    //代付
    default AbstractExcutor createWithdrawExcutor() {
        return null;
    };
    //代付查询
    default AbstractExcutor createWithdrawQueryExcutor() {
        return null;
    };
}
