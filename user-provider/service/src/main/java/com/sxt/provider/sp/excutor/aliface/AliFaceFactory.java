package com.sxt.provider.sp.excutor.aliface;

import com.sxt.commons.utils.SpringContextUtil;
import com.sxt.provider.sp.excutor.AbstractExcutor;
import com.sxt.provider.sp.excutor.ExcutorFactory;
import org.springframework.stereotype.Component;

@Component
public class AliFaceFactory implements ExcutorFactory {


//    @Override
//    public AbstractExcutor createAlipayWapExcutor() {
//        return (AbstractExcutor) SpringContextUtil.getBean(AliFaceWapExcutor.class);
//    }
    @Override
    public AbstractExcutor createAlipayNativeExcutor() {
        return (AbstractExcutor) SpringContextUtil.getBean(AliFaceNativeExcutor.class);
    }
//    @Override
//    public AbstractExcutor createAlipaySettleExcutor() {
//        return (AbstractExcutor) SpringContextUtil.getBean(AlipaySettleExcutor.class);
//    }

}
