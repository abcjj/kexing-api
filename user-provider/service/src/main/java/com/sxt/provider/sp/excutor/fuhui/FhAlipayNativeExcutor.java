package com.sxt.provider.sp.excutor.fuhui;

import com.alibaba.fastjson.JSONObject;
import com.sxt.commons.typeEnum.ApiTypeEnum;
import com.sxt.commons.typeEnum.OrderStateEnum;
import com.sxt.commons.utils.DateUtils;
import com.sxt.commons.utils.SignMD5Utils;
import com.sxt.pojo.entity.Order;
import com.sxt.provider.order.OrderService;
import com.sxt.provider.sp.excutor.AbstractExcutor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class FhAlipayNativeExcutor extends AbstractExcutor {

    @Autowired
    OrderService orderService;

    @Override
    protected ApiTypeEnum getApiTypeEnum() {
        return ApiTypeEnum.ALIPAY_NATIVE;
    }

    @Override
    protected Map<String, Object> getApiParams(Order order, Map<String, Object> spApiConfig, Map<String, Object> extendOrderParams) {
        Map<String, Object> apiParams = new HashMap<>();
        apiParams.put("version", "1");
        apiParams.put("pay_type", "22");
        apiParams.put("agent_bill_id", order.getTradeNo());
        apiParams.put("pay_amt", new BigDecimal(order.getAmount()).divide(new BigDecimal("100"),2,BigDecimal.ROUND_DOWN).toString());
        apiParams.put("user_ip", order.getRemark());
        apiParams.put("agent_bill_time", DateUtils.sdf14.format(new Date()));
        apiParams.put("goods_name", order.getBody());
        apiParams.put("remark", order.getCpId());

        return apiParams;
    }

    @Override
    protected Map<String, Object> run(Map<String, Object> spConfig, Map<String, Object> params) throws Exception {
        String agentId = getVal(spConfig, "agent_id").toString();
        Object notifyUrl= getVal(spConfig, "notify_url");
        Object returnUrl= getVal(spConfig, "return_url");
        Object key= getVal(spConfig, "key");
        Object apiUrl= getVal(spConfig, "api_url");

        params.put("agent_id",agentId);
        params.put("notify_url",notifyUrl);
        params.put("return_url",returnUrl);

        //生成sign
        String sign = SignMD5Utils.getSign(params,key.toString());
        params.put("sign", sign); //区域，若境外可以传HK

        String result = sendHttpRequest(apiUrl.toString(),getParamStr(params),"utf-8");

        if (StringUtils.isBlank(result)) {
            throw new Exception("请求付汇的接口失败");
        }
        Order order = orderService.getByTradeNo(params.get("agent_bill_id").toString());

        Map<String, Object> resultMap = new HashMap<>();
        JSONObject jsonObject = JSONObject.parseObject(result);
        if("1".equals(jsonObject.getString("status"))){
            Map<String, Object> responseMap = JSONObject.parseObject(jsonObject.getString("data"));
            resultMap.put("result_code", "0");
            resultMap.put("code_url", responseMap.get("pay_url"));
            order.setState(OrderStateEnum.WAITING.getType());
        }else{
            resultMap.put("result_code", "1");
            resultMap.put("message", jsonObject.getString("info"));
            order.setState(OrderStateEnum.REQERROR.getType());
        }
        orderService.update(order);
        return resultMap;
    }
}
