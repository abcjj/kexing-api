package com.sxt.provider.sp.excutor.fuhui;

import com.sxt.commons.utils.SpringContextUtil;
import com.sxt.provider.sp.excutor.AbstractExcutor;
import com.sxt.provider.sp.excutor.ExcutorFactory;
import com.sxt.provider.sp.excutor.aliface.AliFaceNativeExcutor;
import org.springframework.stereotype.Component;

@Component
public class FhFactory implements ExcutorFactory {


    @Override
    public AbstractExcutor createUnionNetExcutor() {
        return (AbstractExcutor) SpringContextUtil.getBean(FhUnionNetExcutor.class);
    }
    @Override
    public AbstractExcutor createAlipayNativeExcutor() {
        return (AbstractExcutor) SpringContextUtil.getBean(FhAlipayNativeExcutor.class);
    }

}
