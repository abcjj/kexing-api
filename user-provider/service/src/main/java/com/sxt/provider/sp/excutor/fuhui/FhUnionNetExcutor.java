package com.sxt.provider.sp.excutor.fuhui;

import com.alibaba.fastjson.JSONObject;
import com.sxt.commons.typeEnum.ApiTypeEnum;
import com.sxt.commons.typeEnum.OrderStateEnum;
import com.sxt.commons.utils.DateUtils;
import com.sxt.pojo.entity.Order;
import com.sxt.provider.order.OrderService;
import com.sxt.provider.sp.excutor.AbstractExcutor;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class FhUnionNetExcutor extends AbstractExcutor {

    @Autowired
    OrderService orderService;

    @Override
    protected ApiTypeEnum getApiTypeEnum() {
        return ApiTypeEnum.ALIPAY_NATIVE;
    }

    @Override
    protected Map<String, Object> getApiParams(Order order, Map<String, Object> spApiConfig, Map<String, Object> extendOrderParams) {
        Map<String, Object> apiParams = new HashMap<>();
        apiParams.put("version", "3");
        apiParams.put("is_phone", "1");//是否使用手机触屏版，1=是，PC版请不用传本参数
        apiParams.put("pay_type", "20");
        apiParams.put("pay_code", "0");//支付类型编码 pay_type=20时，pay_code此参数值为0时，则为跳到汇付宝界面选择银行。为银行编码时直接跳转到银行编码对应的银行。
        apiParams.put("bank_card_type", "0");//银行类型：未知=-1，储蓄卡=0，信用卡=1。
        apiParams.put("agent_bill_id", order.getTradeNo());
        apiParams.put("pay_amt", new BigDecimal(order.getAmount()).divide(new BigDecimal("100"),2,BigDecimal.ROUND_DOWN).toString());
        apiParams.put("user_ip", order.getRemark());
        apiParams.put("agent_bill_time", DateUtils.sdf14.format(new Date()));
        apiParams.put("goods_name", order.getBody());
        apiParams.put("remark", order.getCpId());

        return apiParams;
    }

    @Override
    protected Map<String, Object> run(Map<String, Object> spConfig, Map<String, Object> params) throws Exception {
        String agentId = getVal(spConfig, "agent_id").toString();
        Object notifyUrl= getVal(spConfig, "notify_url");
        Object returnUrl= getVal(spConfig, "return_url");
        Object key= getVal(spConfig, "key");
        Object apiUrl= getVal(spConfig, "api_url");

        params.put("agent_id",agentId);
        params.put("notify_url",notifyUrl);
        params.put("return_url",returnUrl);

        //生成sign
        String paramStr = generateSignStr(params,key.toString());
        String sign = DigestUtils.md5Hex(paramStr);
        String requestParam =generateReqStr(params,sign);

        String result = sendHttpRequest(apiUrl.toString(),"POST",requestParam);

        if (StringUtils.isBlank(result)) {
            throw new Exception("请求付汇的接口失败");
        }
        Order order = orderService.getByTradeNo(params.get("agent_bill_id").toString());

        Map<String, Object> resultMap = new HashMap<>();
        JSONObject jsonObject = JSONObject.parseObject(result);
        if("1".equals(jsonObject.getString("status"))){
            Map<String, Object> responseMap = JSONObject.parseObject(jsonObject.getString("data"));
            resultMap.put("result_code", "0");
            resultMap.put("code_url", responseMap.get("pay_url"));
            order.setState(OrderStateEnum.WAITING.getType());
        }else{
            resultMap.put("result_code", "1");
            resultMap.put("message", jsonObject.getString("info"));
            order.setState(OrderStateEnum.REQERROR.getType());
        }
        orderService.update(order);
        return resultMap;
    }

    /**
     * 生成签名串
     * @param params
     * @return
     */
    private String generateSignStr(Map<String, Object>params,String key){
        return "version="+params.get("version")+"&agent_id="+params.get("agent_id")+"&agent_bill_id="+params.get("agent_bill_id")
                +"&agent_bill_time="+params.get("agent_bill_time")+"&pay_type="+params.get("pay_type")+"&pay_amt="+params.get("pay_amt")
                +"&notify_url="+params.get("notify_url")+"&return_url="+params.get("return_url")+"&user_ip="+params.get("user_ip")
                +"&bank_card_type="+params.get("bank_card_type")
                +"&key="+key;

    }
    /**
     * 生成请求
     * @param params
     * @return
     */
    private String generateReqStr(Map<String, Object>params,String sign){
        try {
            return "version="+params.get("version")+"&agent_id="+params.get("agent_id")+"&agent_bill_id="+params.get("agent_bill_id")
                    +"&agent_bill_time="+params.get("agent_bill_time")+"&pay_type="+params.get("pay_type")+"&pay_amt="+params.get("pay_amt")
                    +"&notify_url="+params.get("notify_url")+"&return_url="+params.get("return_url")+"&user_ip="+params.get("user_ip")
                    +"&goods_name="+URLEncoder.encode(params.get("goods_name").toString(),"GBK")
                    +"&remark="+ URLEncoder.encode(params.get("remark").toString(),"GBK")
                    +"&is_phone="+params.get("is_phone")+"&pay_code="+params.get("pay_code")+"&bank_card_type="+params.get("bank_card_type")
                    +"&sign="+sign;
        }catch (Exception e){
            logger.info("生成请求错误："+e.getMessage());
            return "";
        }
    }
}
