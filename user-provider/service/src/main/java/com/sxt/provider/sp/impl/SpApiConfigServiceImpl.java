package com.sxt.provider.sp.impl;

import com.sxt.commons.typeEnum.ApiTypeEnum;
import com.sxt.commons.typeEnum.ChannelTypeEnum;
import com.sxt.dao.mapper.SpApiConfigMapper;
import com.sxt.pojo.entity.SpApiConfig;
import com.sxt.provider.sp.SpApiConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SpApiConfigServiceImpl implements SpApiConfigService {

    @Autowired
    SpApiConfigMapper spApiConfigMapper;


    @Override
    public Map<String, Object> getKeyValues(Long spid,
                                            ChannelTypeEnum channelTypeEnum,
                                            ApiTypeEnum apiTypeEnum) {

        List<SpApiConfig> spApiConfigs =
                spApiConfigMapper.getKeyValues(
                        spid,
                        channelTypeEnum.getType(),
                        apiTypeEnum.getType());
        if(spApiConfigs == null || spApiConfigs.size() == 0)
            return new HashMap<>();

        Map<String, Object> result = new HashMap<>();
        spApiConfigs.stream().forEach(p->{
            result.put(p.getKey(), p.getVal());
        });
        return result;
    }

    @Override
    public List<SpApiConfig> all() {
        return null;
    }

    @Override
    public List<SpApiConfig> selectBySpId(int id) {
        return spApiConfigMapper.selectBySpId(id);
    }


}
