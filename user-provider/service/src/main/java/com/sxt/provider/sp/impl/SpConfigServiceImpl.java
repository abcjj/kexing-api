package com.sxt.provider.sp.impl;

import com.sxt.dao.mapper.SpConfigMapper;
import com.sxt.pojo.entity.SpConfig;
import com.sxt.provider.sp.SpConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SpConfigServiceImpl implements SpConfigService {

    @Autowired
    SpConfigMapper spConfigMapper;

    @Override
    public Map<String, Object> getKeyValues(Long spid) {
        List<SpConfig> spConfigs = spConfigMapper.getKeyValues(spid);
        if(spConfigs == null || spConfigs.size() == 0)
            return new HashMap<>();

        Map<String, Object> result = new HashMap<>(spConfigs.size());
        spConfigs.stream().forEach(p->{
            result.put(p.getKey(), p.getVal());
        });
        return result;
    }

    @Override
    public String getValue(Long spid, String key) {
        SpConfig spConfig = spConfigMapper.getValue(spid, key);
        if (spConfig == null)
            return null;

        return spConfig.getVal();
    }

    @Override
    public List<SpConfig> getBySpId(Long spid) {
        return spConfigMapper.getKeyValues(spid);
    }
}
