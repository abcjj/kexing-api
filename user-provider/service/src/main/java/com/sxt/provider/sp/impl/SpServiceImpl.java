package com.sxt.provider.sp.impl;


import com.sxt.commons.utils.SpringContextUtil;
import com.sxt.dao.mapper.SpMapper;
import com.sxt.pojo.entity.Sp;
import com.sxt.provider.sp.SpService;
import com.sxt.provider.sp.excutor.ExcutorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpServiceImpl implements SpService {
    @Autowired
    SpMapper spMapper;
    @Override
    public ExcutorFactory getFactory(Long spid)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Sp sp = spMapper.selectById(spid);
        return getFactory(sp);
    }

    @Override
    public ExcutorFactory getFactory(Sp sp) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        ExcutorFactory excutorFactory =
                (ExcutorFactory) SpringContextUtil.getBean(Class.forName(sp.getFactoryClassName()));
        return excutorFactory;
    }
}
