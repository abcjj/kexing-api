package com.sxt.test.provider;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestProvider {
	
	public static void main(String[] args) throws Exception {
		
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("classpath:com/sxt/config/spring/*.xml");
		
		context.start();
		
		System.out.println("provider started..");
		
		System.in.read();
		
	}

}
